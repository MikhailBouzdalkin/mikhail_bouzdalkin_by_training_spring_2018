package by.training.reflection;


import by.training.reflection.annotations.Serializable;
import by.training.reflection.serializers.Serializer;

import java.util.logging.Logger;

/**
 * Runner class that serialize object of class User into Map
 * and print this Map.
 */
public class Runner {


    private static final String USER_NAME = "Ivan";
    private static final String USER_PASSWORD = "qwerty123";
    private static final String USER_EMAIL = "Ivan@gmail.com";
    private static Logger log = Logger.getLogger(Runner.class.getName());

    /**
     * Information class which will be serialized.
     */
    @Serializable()
    public static class User {
        @Serializable()
        private String login;
        private String password;
        @Serializable(alias = "mail")
        private String email;

        @Override
        public String toString() {
            return "User{" +
                    "login='" + login + '\'' +
                    ", password='" + password + '\'' +
                    ", email='" + email + '\'' +
                    '}';
        }

        /**
         * Default public constructor.
         */
        public User() {
        }

        /**
         * Public constructor
         *
         * @param login    - user login
         * @param password - user password
         * @param email    - user email
         */
        public User(String login, String password, String email) {
            this.login = login;
            this.password = password;
            this.email = email;
        }

    }

    public static void main(String[] args) throws IllegalAccessException {
        log.info((Serializer.serialize(new User(USER_NAME, USER_PASSWORD, USER_EMAIL))).toString());
    }


}
