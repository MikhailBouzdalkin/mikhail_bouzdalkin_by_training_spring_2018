package by.training.reflection.serializers;



import by.training.reflection.annotations.Serializable;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Unified objects serializer and deserializer.
 * The utility class gets object, serializes it
 * to store in Map structure and can recover objects from Map.
 */
public class Serializer {
    /**
     * Method to serialize object in Map.
     *
     * @param o - object that you want to serialize.
     * @return Map - where key is the name of the class or field,
     * or its alias, and the value is the value of the field in
     * string representation
     */

    private static final String CLASS_KEY = "Class";

    public  static Map<String, String> serialize(final Object o) throws IllegalAccessException {
        Map<String, String> map = new HashMap<>();
        Class clss = o.getClass();
        if (clss.isAnnotationPresent(Serializable.class)) {
            Serializable serializable = (Serializable) clss.getAnnotation(Serializable.class);
            map.put(CLASS_KEY, clss.getName());
        }
        Field[] fields;
        Field[] clssFields = clss.getDeclaredFields();
        Class superclss= clss.getSuperclass();
        if(superclss.isAnnotationPresent(Serializable.class)){
            Field[] superclssFields = superclss.getDeclaredFields();
            fields=Arrays.copyOf(clssFields, clssFields.length + superclssFields.length);
            System.arraycopy(superclssFields, 0, fields, clssFields.length, superclssFields.length);
        }else {
            fields =clssFields;
        }

        for (Field field : fields) {
            if (field.isAnnotationPresent(Serializable.class)) {
                field.setAccessible(true);
                String value = String.valueOf(field.get(o));
                String alias = field.getAnnotation(Serializable.class).alias();
                if (alias.isEmpty()) {
                     map.put(field.getName(), value);
                } else {
                   map.put(alias, value);
                }
            }
        }
        return map;
    }



    /**
     * Method to serialize object in Map.
     *
     * @param map - where key is the name of the class or field,
     *            or its alias, and the value is the value of the field in
     *            string representation
     * @return - deserialized object.
     */
    public static Object deserialize(final Map<String, String> map) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> clss= Class.forName(map.get(CLASS_KEY));
        Object o= clss.newInstance();
        Field[] fields;
        Field[] clssFields = clss.getDeclaredFields();
        Class superclss= clss.getSuperclass();
        if(superclss.isAnnotationPresent(Serializable.class)){
        Field[] superclssFields = superclss.getDeclaredFields();
             fields=Arrays.copyOf(clssFields, clssFields.length + superclssFields.length);
            System.arraycopy(superclssFields, 0, fields, clssFields.length, superclssFields.length);
        }else {
            fields =clssFields;
        }

        for (Field field : fields) {
            if (field.isAnnotationPresent(Serializable.class)) {
                field.setAccessible(true);
                String alias = field.getAnnotation(Serializable.class).alias();
                if (alias.isEmpty()) {
                    field.set(o,map.get(field.getName()));
                } else {
                field.set(o,map.get(alias));}
            }
        }
     return o;
    }
}
