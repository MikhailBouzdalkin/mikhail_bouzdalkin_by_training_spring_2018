package by.training.reflection.factories;



import by.training.reflection.annotations.Proxy;
import by.training.reflection.invocationhandlers.SomeInvocationHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

/**
 * The class is used to get an object instance for a class provided as attribute.
 * */
public class Factory {
  private static final Logger log = Logger.getLogger(SomeInvocationHandler.class.getName());
  /**
   *  Method to get an object for the specified class.
   *  If the class has the @by.training.annotations.Proxy annotation applied then
   *  a proxy object will be returned instead of the common instance.
   * @param clss - Class whose instance returns a method.
   * @return - Instance of class or instance of proxy class.
   * When an exception is thrown, it returns a null.
   *  */
  public static Object getInstanceOf(Class<?> clss) {
    try {
      if (clss.isAnnotationPresent(Proxy.class)) {
        Proxy proxy = clss.getAnnotation(Proxy.class);
        String invocationHandler = proxy.invocationHandler();
        return java.lang.reflect.Proxy.newProxyInstance(clss.getClassLoader(), clss.getInterfaces(),
                (InvocationHandler) Class.forName(invocationHandler).getConstructor().newInstance());

      } else return clss.newInstance();

    } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
            NoSuchMethodException | ClassNotFoundException e) {
         log.info(e.getMessage());
      return null;

    }
  }
}
