package by.training.reflection.invocationhandlers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.logging.Logger;

/**
 * Handler that print message and invoke original method.
 */
public class SomeInvocationHandler implements InvocationHandler {
    private final Logger log = Logger.getLogger(SomeInvocationHandler.class.getName());
    private Object obj;

    /**
     * Public constructor.
     * @param object - object whose methods will be called in invoke() method.
     */
    public SomeInvocationHandler(final Object object) {
        obj = object;
    }

    public SomeInvocationHandler() {
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        log.info("by.training.annotations.Proxy!");
        return method.invoke(obj, method);

    }
}
