package by.training.servlet.beans;

import by.training.reflection.annotations.Serializable;

@Serializable
public abstract class Data {
    @Serializable
    private  String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Data(String id) {
        this.id = id;
    }

    public Data() {
    }
}
