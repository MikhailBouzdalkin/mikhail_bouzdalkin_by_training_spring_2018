package by.training.servlet.beans;

import by.training.reflection.annotations.Serializable;

@Serializable
public class UserData extends Data {

    @Serializable
    private String name;
    @Serializable
    private String surname;



    public UserData() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' + ", id="+getId()+
                '}';
    }
}
