package by.training.servlet.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.Map;

import by.training.servlet.businesslogic.BusinessLogic;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@WebServlet("/data/*")
public class MainServlet extends HttpServlet {
    private static final String KEY_ERROR = "error";
    private static final String ATTR_ERROR_JSON_EMPTY = "JSON file empty or not found.";
    private static final String ATTR_ERROR_ILLEGAL_ACCESS = "Parsing error illegal access to field.";
    private static final String ATTR_ERROR_INSTATIATING = "Cannot instantiating object from JSON.";
    private static final String ATTR_ERROR_WRONG_CLASS_NAME = "Wrong class name in JSON.";
    private static final String PATH_DELIMITER = "/";
    private static final String ERROR_WRITING_JSON = "Error writing JSON";

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(MainServlet.class);

    ApplicationContext springAppLicationContext = new ClassPathXmlApplicationContext("spring.xml");
    BusinessLogic businessLogic = (BusinessLogic) springAppLicationContext.getBean("businessLogic");
    Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.equals(PATH_DELIMITER)) {
        }
        String[] splits = pathInfo.split(PATH_DELIMITER);
        if (splits.length != 2) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        String dataId = splits[1];
        Map<String, String> dataMap = null;
        try {
            dataMap = businessLogic.getFromCacheAndSerialize(dataId);
            sendJson(response, dataMap);
        } catch (IllegalAccessException e) {
        } catch (NullPointerException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = request.getReader();
            String jsonFileLine;
            while ((jsonFileLine = reader.readLine()) != null) {
                buffer.append(jsonFileLine);
            }
            String jsonString = buffer.toString();
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            Map<String, String> dataMap = gson.fromJson(jsonString, type);

            try {
                businessLogic.deserializeAndPutInCache(dataMap);
            } catch (NullPointerException e) {
                log.error(ATTR_ERROR_JSON_EMPTY, e);
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (IllegalAccessException e) {
                log.error(ATTR_ERROR_ILLEGAL_ACCESS, e);
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (InstantiationException e) {
                log.error(ATTR_ERROR_INSTATIATING, e);
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            } catch (ClassNotFoundException e) {
                log.error(ATTR_ERROR_WRONG_CLASS_NAME, e);
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }

        }
    }

    private void sendJson(HttpServletResponse response, Map<String, String> dataMap) {
        String jsonString = gson.toJson(dataMap);
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter();) {
            out.print(jsonString);
            out.flush();
        } catch (IOException e) {
            log.error(ERROR_WRITING_JSON, e);
        }
    }
}
