package by.training.servlet.businesslogic;

import by.training.caches.Cache;
import by.training.caches.LRUCache;
import by.training.servlet.beans.Data;
import by.training.reflection.serializers.Serializer;

import java.util.Map;

/**
 * Class represents business logic of application.
 */
public class BusinessLogic {
    private Serializer serializer;
    private Cache<String,Data> cache;

    /**
     * Constructor.
     * @param cache - used cache (LRU or LFU).
     */
    public BusinessLogic(Cache cache) {
        serializer=new Serializer();
        this.cache =cache;

    }

    /**
     * Method for deserialization and put object in cache.
     * @param serializedObject - object serialized in Map.
     * @throws IllegalAccessException - if serializer cannot accaess to fields of data object.
     * @throws InstantiationException - if serializer cannot instantiate object of class which name is
     * element with "Class" key in Map.
     * @throws ClassNotFoundException - if Map is empty or null.
     * @throws NullPointerException - if serializer returns null.
     */
    public void deserializeAndPutInCache(Map<String,String> serializedObject) throws IllegalAccessException,
            InstantiationException, ClassNotFoundException, NullPointerException {
            Data deserializedObject= (Data) serializer.deserialize(serializedObject);
            cache.putInCache(deserializedObject.getId(),deserializedObject);
        }

    /**
     * Method for get object from cache and serialization this object.
     * @param key - id of data object.
     * @return - data object.
     * @throws IllegalAccessException - if serializer cannot accaess to fields of data object.
     */
        public Map<String,String> getFromCacheAndSerialize(String key) throws IllegalAccessException {
         Data data =cache.getFromCache(key);
         return serializer.serialize(data);

     }
}
