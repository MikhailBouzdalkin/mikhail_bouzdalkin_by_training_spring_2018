package by.training.caches;

import java.util.Hashtable;
import java.util.LinkedList;

/**
 * Class is the lfu cache
 * It requires three data structures. One is a hash table <b>cache</b>
 * which is used to cache the key/values so
 * that given a key we can retrieve the cache entry at O(1).
 * Second one is a double linked list for each frequency of access.
 * The max frequency is capped at the cache size to avoid
 * creating more and more frequency list entries. Each frequency
 * will have a double linked list to keep track of the cache
 * entries belonging to that particular frequency.
 * The third data structure is double linked list
 * <b>frequencies</b>to link these frequencies lists.
 *
 * @param <K> - object of this class will be used as a key
 * @param <V> - object of this class will be the information stored in cache
 *           stored in the cache
 * @version 1.0
 * @author Mikhail Bouzdalkin
 */

public class LFUCache<K, V> implements Cache<K,V>{

    private static final int DEFAULT_CAPACITY=16;
    /**field cache*/
    private Hashtable<K, Node<V>> cache;
    /**field frequencies is list of lists when zero LinkedList stored keys with c number of calls 0 etc.*/
    private LinkedList<LinkedList<K>> frequencies;
    /**field capacity - maximum cache size*/
    private int capacity;

    /**
     * public constructor
     *
     * @param cap - cache capacity
     */
    public LFUCache(final int cap) {
        this.capacity = cap;
        initCache();
    }

    public LFUCache() {
        this.capacity=DEFAULT_CAPACITY;
    initCache();
    }

    /**
     * Initialization method.
     */
    private void initCache(){
        cache = new Hashtable<>();
        frequencies = new LinkedList<LinkedList<K>>();
        for (int i = 0; i < capacity; i++) {
            frequencies.add(new LinkedList<K>());
        }
    }

    /**
     * Nested class is servlet that stores
     * information placed in the cache<b>value</b> and number of Linked
     * which indicates which list it is based on the frequency
     * of access to it<b>bucket</b>
     *
     * @param <V> - object of this class will be the information stored in cache
     */
     private final class Node<V> {
        /** field value*/
        private V value;
        /** field bucket which indicates the number of calls*/
        private int bucket;

        /**
         * public constructor
         *
         * @param value - value which will be stored in this servlet
         */
        private Node(final V value) {
            this.value = value;

        }
    }
    /**
     * Method to get information stored in cache by key

     * @param key - key
     * @return object associated with this key
     *
     * When requested to get the item the key falls into the next
     * LinkedList corresponding to the frequency one more.
     * The number of frequencies is limited by the size of the
     * cache so it is checked whether this item is
     * in the last LinkedList.
     */
    public V getFromCache(final K key) {
        Node<V> node = cache.get(key);
        if (node != null) {
            if (node.bucket < capacity - 1) {
                LinkedList<K> bucketList = frequencies.get(node.bucket);
                int position = bucketList.indexOf(key);
                frequencies.get(++node.bucket).
                        add(bucketList.get(position));
                bucketList.remove(position);
            }
            return node.value;
        }
        return null;
    }

    /**
     * Method to put information in cache
     *
     * @param key   - key
     * @param value - value associated with this key
     *
     * When the cache is full, a non-empty LinkedList
     * with minimum frequency is searched for and the last added element.
     */

    public void putInCache(final K key, final V value) {
        if (cache.size() == capacity) {
            for (LinkedList<K> list : frequencies) {
                if (!list.isEmpty()) {
                    cache.remove(list.getLast());
                    list.removeLast();
                    break;
                }
            }
        }
        LinkedList<K> firstBucket = frequencies.getFirst();
        cache.put(key, new Node<>(value));
        firstBucket.add(key);

    }

}
