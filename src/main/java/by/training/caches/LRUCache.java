package by.training.caches;

import java.util.LinkedHashMap;
import java.util.Map;

/**Class is the lfu cache.
 *
 * @param <K> - object of this class will be used as a key
 * @param <V> - object of this class will be the information stored in the cache
 * @author Mikhail Bouzdalkin
 * @version 1.0
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V>  implements  Cache<K,V>{
    /**field capacity - maximum cache size*/
    private int maxSize = 16;

    /**
     * method to get maximum cache size
     * @return cache maximum size
     */
    public int getMaxSize() {
        return maxSize;
    }

    /**
     * public constructor
     *
     * @param maxSize - cache capacity
     */
    public LRUCache(final int maxSize) {
        this.maxSize = maxSize;
    }

    /**
     * default public constructor
     */
    public LRUCache() {

    }

    /**
     * method to change maximum cache size
     *
     * @param maxSize - cache capacity
     */
    public void setMaxSize(final int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    protected boolean removeEldestEntry(final Map.Entry eldest) {
        synchronized (this) {
            return size() > maxSize;
        }

    }

    @Override
    public V getFromCache(K key) {
        return this.get(key);
    }
    @Override
    public void putInCache(K key, V value){
        put(key,value);
    }


}
