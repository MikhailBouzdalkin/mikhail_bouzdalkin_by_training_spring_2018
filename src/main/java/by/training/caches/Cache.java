package by.training.caches;

public interface Cache<K,V> {

/**
 * Method to get information stored in cache by key.
 * @param key - key.
 * @return object associated with this key.
 */
    public V getFromCache(K key);
    /**
     * Method to put information in cache
     *
     * @param key   - key
     * @param value - value associated with this key
     */
    public void putInCache(K key,V value);

}
