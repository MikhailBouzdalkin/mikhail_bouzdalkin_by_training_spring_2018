package by.training.junit;

/**
 * DAO interface which uses String objects as for keys as for values.
 * Allows to add, delete and get data from data source.
 */
public interface IDAO {
    /**
     * Add data to data source.
     * @param key - String key.
     * @param value - String value associated to this key.
     */
    void put(String key, String value);

    /**
     * Get data from data source.
     * @param key - String key on which the value will be searched in data source.
     * @return
     */
    String get(String key);

    /**
     * Remove data from data source
     * @param key - String on which the element will be searched and deleted.
     */
    void remove(String key);
}
