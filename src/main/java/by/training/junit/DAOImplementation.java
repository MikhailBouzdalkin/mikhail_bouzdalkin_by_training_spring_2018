package by.training.junit;

import java.util.HashMap;
import java.util.Map;

/**
 * DAO implementation which uses Map as a data source.
 */
public class DAOImplementation implements IDAO {
    /**
     * Data source - HashMap.
     */
    private Map<String, String> map = new HashMap<>();

    @Override
    public void put(String key, String value) {
        map.put(key, value);
    }

    @Override
    public String get(String key) {
        return map.get(key);
    }

    @Override
    public void remove(String key) {
        map.remove(key);
    }
}
