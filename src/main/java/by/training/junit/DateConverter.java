package by.training.junit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class which performs operations with Date.
 */
public class DateConverter {
    /**
     * Date formatter.
     */
    private SimpleDateFormat format;

    /**
     * Public constructor.
     * @param pattern - pattern used for converting date to string.
     */
    public DateConverter(String pattern) {
        format = new SimpleDateFormat(pattern);
    }


    /**
     * converts String value to Date, using patterns which are
     * passed to the constructor. If argument type has null value, it
     * throws an exception.
     * @param date - String that converts to Date.
     * @return date.
     * @throws IllegalArgumentException
     */
    public Date getDate(String date) throws IllegalArgumentException {
        try {
            return format.parse(date);
        } catch (ParseException | NullPointerException e) {
            throw new IllegalArgumentException(e.getCause());
        }
    }

    /**
     * Return String date representation of the Date according to the pattern,
     * if pattern was not provided in the constructor, it throws an exception.
     * @param date - date that converts to String.
     * @return date in String format.
     * @throws IllegalArgumentException
     */
    public String getDateString(Date date) throws IllegalArgumentException {
        if (date == null) {
            throw new IllegalArgumentException();
        }
        return format.format(date);
    }
}
