package by.training.junit;

/**
 * Class which receives Strings from DAO, modifies them (
 * concatenates with initial value) and stores with DAO.
 */
public class StringUtil {
    /**
     * Data source
     */
    private IDAO dao;

    /**
     * Method that receives Strings from DAO, concatenates with
     * initial value and stores with DAO.
     * @param key - String key of value ​that will be modified.
     * @param concatWith - String to be added to the value.
     */
    public void modify(String key, String concatWith) {
        String result = dao.get(key).concat(concatWith);
        dao.remove(key);
        dao.put(key, result);
    }

    /**
     * Public cnstructor.
     * @param dao - Data source.
     */
    public StringUtil(IDAO dao) {
        this.dao = dao;
    }
}
