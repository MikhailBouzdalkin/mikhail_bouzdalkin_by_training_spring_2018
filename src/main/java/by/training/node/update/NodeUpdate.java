package by.training.node.update;

import by.training.node.beans.ShardInfo;
import by.training.node.businesslogic.NodeBusinessLogic;
import by.training.node.commitlog.CommitLog;
import by.training.node.commitlog.Operation;
import by.training.node.exсeptions.CommitLogException;
import by.training.node.exсeptions.DAOException;
import by.training.node.exсeptions.WrongInputDataException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class NodeUpdate {
    private static final String URL_GET_COMMITS_FROM_REPLICATION =
            "http://%s:%s/commitlog/commitsafter?lastcommit=%d";
    private static final String URL_GET_LAST_COMMIT_NUMBER_FROM_REPLICATION =
            "http://%s:%s/commitlog/lastcommitnumber";
    private static final String REQUEST_METHOD_GET = "GET";
    private static final int RESPONSE_CODE_OK = 200;

    private static final String COMMIT_LOG_LINE_DELIMITER = " / ";
    private static final int OPERATION_NUMBER_IN_COMMIT_LOG_LINE = 2;
    private static final int COLLECTION_NAME_NUMBER_IN_COMMIT_LOG_LINE = 3;
    private static final int OBJECT_ID_NUMBER_IN_COMMIT_LOG_LINE = 4;
    private static final int JSON_SCHEMA_NUMBER_IN_COMMIT_LOG_LINE = 4;
    private static final int JSON_OBJECT_NUMBER_IN_COMMIT_LOG_LINE = 5;


    private static final String ERROR_READING_FROM_RESPONSE = "Cannot read commits from response.";
    private static final String ERROR_PARSE_COMMIT_NUMBER_FROM_STRING = "Cannot parse commit number from string: ";
    private static final String ERROR_GET_LAST_COMMIT_FROM_COMMIT_LOG = "Cannot get last commit from commit log.";
    private static final String ERROR_GET_LAST_COMMIT_REQUEST = "Shard doesn't active: cannot connect when get last commit from " +
            "replication: ";
    private static final String ERROR_GET_LAST_COMMITS = "Wrong request get commits from replication.";
    private static final String ERROR_UPDATE_SHARD = "Cannot update shard state.";

    private static final String KEY_DATE = "date";
    private static final String KEY_COMMIT_NUMBER = "commitNumber";
    private static final String KEY_OPERATION = "operation";
    private static final String KEY_COLLECTION = "collection";
    private static final String KEY_OBJECT_ID = "objectId";
    private static final String KEY_JSON_DATA = "jsonData";
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(NodeUpdate.class);
    private static final Gson gson = new Gson();
    private NodeBusinessLogic nodeBusinessLogic;
    private CommitLog commitLog;

    public NodeUpdate(NodeBusinessLogic nodeBusinessLogic, CommitLog commitLog) {
        this.nodeBusinessLogic = nodeBusinessLogic;
        this.commitLog = commitLog;
    }

    /**
     * Method for update shard if other shard in the node has later commit.
     *
     * @param shardInfos - information of shards(host and port).
     */
    public void updateFromReplicationsCommitLog(List<ShardInfo> shardInfos) {
        /**
         * Class used for link port number and last commit number of replication.
         */
        class LastCommit implements Comparable<LastCommit> {
            long lastCommitNumber;
            String host;
            String port;

            public LastCommit(long lastCommitNumber, String host, String port) {
                this.lastCommitNumber = lastCommitNumber;
                this.host = host;
                this.port = port;
            }

            public long getLastCommitNumber() {
                return lastCommitNumber;
            }

            public String getHost() {
                return host;
            }

            public String getPort() {
                return port;
            }

            @Override
            public int compareTo(LastCommit o) {
                long objectLastCommitNumber = o.getLastCommitNumber();
                int result = 1;
                if (lastCommitNumber == objectLastCommitNumber) {
                    return result = 0;
                } else {
                    if (lastCommitNumber < objectLastCommitNumber) {
                        result = -1;
                    }
                }
                return result;
            }


        }

        try {

            long nodeLastCommit = commitLog.getLastCommitNumber();
            List<LastCommit> replicationsLastCommits = new ArrayList<>();
            for (ShardInfo shardInfo : shardInfos) {
                try {
                    HttpURLConnection connection = openConnection(String.format(
                            URL_GET_LAST_COMMIT_NUMBER_FROM_REPLICATION, shardInfo.getHost(), shardInfo.getPort(),
                            nodeLastCommit));

                    int responseCode = connection.getResponseCode();
                    if (responseCode == RESPONSE_CODE_OK) {
                        try {
                            replicationsLastCommits.add(new LastCommit(
                                    readLastCommitFromResponse(connection), shardInfo.getHost(), shardInfo.getPort()));
                        } catch (ConnectException e) {
                            log.error(ERROR_READING_FROM_RESPONSE);

                        } catch (IOException e) {
                            log.error(ERROR_READING_FROM_RESPONSE);
                        } catch (NumberFormatException e) {
                            log.error(ERROR_PARSE_COMMIT_NUMBER_FROM_STRING, e);
                        }
                    }
                } catch (IOException e) {
                    log.error(ERROR_GET_LAST_COMMIT_REQUEST + shardInfo, e);
                }
            }
            if (replicationsLastCommits.size() != 0) {
                LastCommit latestCommit = Collections.max(replicationsLastCommits);
                if (latestCommit.getLastCommitNumber() > nodeLastCommit) {
                    String latestCommitHost = latestCommit.getHost();
                    String latestCommitPort = latestCommit.getPort();

                    if (latestCommitHost != null && latestCommitPort != null) {
                        HttpURLConnection connection = openConnection
                                (String.format(URL_GET_COMMITS_FROM_REPLICATION, latestCommitHost,
                                        latestCommitPort, nodeLastCommit));
                        updateShard(connection);
                    }
                }
            }

        } catch (CommitLogException e) {
            log.error(ERROR_GET_LAST_COMMIT_FROM_COMMIT_LOG, e);
        } catch (IOException e) {
            log.error(ERROR_GET_LAST_COMMITS, e);
        }

    }

    /**
     * Method for get last commit from request.
     *
     * @param connection - http connection.
     * @return last commit number of shard.
     * @throws IOException - if an error occurred while reading from the response.
     */
    private long readLastCommitFromResponse(HttpURLConnection connection) throws IOException {
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            long lastCommitNumber = Long.parseLong(response.toString());
            return lastCommitNumber;
        }
    }

    /**
     * Method for open http connection for get request.
     *
     * @param stringUrl - url of request in string format.
     * @return connection
     * @throws IOException if an error occurred while connection established.
     */
    private HttpURLConnection openConnection(String stringUrl) throws IOException {
        URL url = new URL(stringUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(REQUEST_METHOD_GET);
        return connection;
    }

    /**
     * Method for update shard state from response from replication with later commit number.
     *
     * @param connection - http connection with replication
     * @throws IOException - if an error occurred while reading from the response.
     */
    private void updateShard(HttpURLConnection connection) throws IOException {
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                try {
                    Type type = new TypeToken<Map<String, String>>() {
                    }.getType();
                    Map<String, String> commitMap = gson.fromJson(inputLine, type);
                    Operation operation = Operation.valueOf(commitMap.get(KEY_OPERATION));
                    switch (operation) {
                        case CREATE_OBJECT:
                            nodeBusinessLogic.createObject
                                    (commitMap.get(KEY_COLLECTION),
                                            commitMap.get(KEY_JSON_DATA));
                            break;
                        case UPDATE_OBJECT:
                            nodeBusinessLogic.updateObject
                                    (commitMap.get(KEY_COLLECTION), commitMap.get(KEY_OBJECT_ID),
                                            commitMap.get(KEY_JSON_DATA));
                            break;
                        case DELETE_OBJECT:
                            nodeBusinessLogic.deleteObject
                                    (commitMap.get(KEY_COLLECTION), commitMap.get(KEY_OBJECT_ID));
                            break;
                        case CREATE_COLLECTION:
                            nodeBusinessLogic.createCollection
                                    (commitMap.get(KEY_COLLECTION),
                                            commitMap.get(KEY_JSON_DATA));
                            break;
                        case UPDATE_COLLECTION:
                            nodeBusinessLogic.updateCollection
                                    (commitMap.get(KEY_COLLECTION),
                                            commitMap.get(KEY_JSON_DATA));
                            break;
                        case DELETE_COLLECTION:
                            nodeBusinessLogic.deleteCollection
                                    (commitMap.get(KEY_COLLECTION));
                            break;
                    }


                } catch (DAOException e) {
                    log.error(ERROR_UPDATE_SHARD, e);
                } catch (WrongInputDataException e) {
                    log.warn(ERROR_UPDATE_SHARD, e.getMessage());
                }
            }


        }
    }


}
