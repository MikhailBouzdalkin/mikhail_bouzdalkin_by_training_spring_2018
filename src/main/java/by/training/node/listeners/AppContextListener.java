package by.training.node.listeners;

import by.training.node.beans.ShardInfo;
import by.training.node.servlets.CollectionsServlet;
import by.training.node.update.NodeUpdate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@WebListener
public class AppContextListener implements ServletContextListener {
    private static final String CLUSTER_CONFIG_FILE = "/WEB-INF/classes/cluster.properties";
    private static final String KEY_PORT = "port";
    private static final String KEY_NODE_ID = "nodeid";
    private static final String KEY_CLUSTER = "cluster";
    private static final String KEY_REPLICATIONS = "replications";
    private static final String KEY_SPRING_CONTEXT = "springContext";
    private static final String ERROR_CONFIG_FILE_NOT_FOUND = "Configuration file not found";
    private static final String ERROR_READ_CONFIG_FILE = "Error while reading properties file";
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(CollectionsServlet.class);
    private static String appRealPath;
    private Gson gson = new Gson();

    public static String getAppRealPath() {
        return appRealPath;
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext ctx = sce.getServletContext();
        ApplicationContext springApplicationContext = new ClassPathXmlApplicationContext("spring.xml");
        NodeUpdate nodeUpdate = (NodeUpdate) springApplicationContext.getBean("nodeUpdate");
        try (InputStream input = ctx.getResourceAsStream(CLUSTER_CONFIG_FILE)) {
            Properties prop = new Properties();
            prop.load(input);
            String stringNodeId = prop.getProperty(KEY_NODE_ID);
            int nodeId = Integer.parseInt(stringNodeId);
            ctx.setAttribute(KEY_NODE_ID, new Integer(nodeId));
            String port = prop.getProperty(KEY_PORT);
            ctx.setAttribute(KEY_PORT, port);
            String jsonNodes = prop.getProperty(KEY_CLUSTER);
            Type clusterType = new TypeToken<Map<Integer, List<ShardInfo>>>() {
            }.getType();
            Map<Integer, List<String>> cluster = gson.fromJson(jsonNodes, clusterType);
            ctx.setAttribute(KEY_CLUSTER, cluster);
            Type replicationsType = new TypeToken<List<ShardInfo>>() {
            }.getType();
            List<ShardInfo> replications = gson.fromJson(prop.getProperty(KEY_REPLICATIONS), replicationsType);
            ctx.setAttribute(KEY_REPLICATIONS, replications);
            ctx.setAttribute(KEY_SPRING_CONTEXT, springApplicationContext);
            nodeUpdate.updateFromReplicationsCommitLog(replications);

        } catch (FileNotFoundException e) {
            log.error(ERROR_CONFIG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            log.error(ERROR_READ_CONFIG_FILE, e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }


}
