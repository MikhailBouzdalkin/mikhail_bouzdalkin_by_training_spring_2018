package by.training.node.commitlog;

import by.training.node.exсeptions.CommitLogException;

import java.util.ArrayList;

public interface CommitLog {
    /**
     * Logging create collection operation in commit log.
     *
     * @param collectionName - name of collection.
     * @param jsonSchema     - JSON schema of objects which contains collection.
     * @throws - if an error occurred when writing to the log.
     */
    public void logCreateCollection(String collectionName, String jsonSchema) throws CommitLogException;

    /**
     * Logging read collection operation in commit log.
     *
     * @param collectionName - name of collection.
     * @throws - if an error occurred when writing to the log.
     */
    public void logReadCollection(String collectionName) throws CommitLogException;

    /**
     * Logging update collection operation in commit log.
     *
     * @param collectionName - name of collection.
     * @param newJsonSchema  - updated JSON schema of objects which contains collection.
     * @throws - if an error occurred when writing to the log.
     */
    public void logUpdateCollection(String collectionName, String newJsonSchema) throws CommitLogException;

    /**
     * Logging delete collection operation in commit log.
     *
     * @param collectionName - name of collection.
     * @throws - if an error occurred when writing to the log.
     */
    public void logDeleteCollection(String collectionName) throws CommitLogException;

    /**
     * Logging create object operation in commit log.
     *
     * @param collectionName - name of collection.
     * @param id             - id of object.
     * @param jsonObject     - object in JSON string format.
     * @throws - if an error occurred when writing to the log.
     */
    public void logCreateObject(String collectionName, int id, String jsonObject) throws CommitLogException;

    /**
     * Logging read object operation in commit log.
     *
     * @param collectionName - name of collection.
     * @param id             - id of object.
     * @throws - if an error occurred when writing to the log.
     */
    public void logReadObject(String collectionName, int id) throws CommitLogException;

    /**
     * Logging update object operation in commit log.
     *
     * @param collectionName - name of collection.
     * @param id             - id of object.
     * @param jsonObject     - object in JSON string format.
     * @throws - if an error occurred when writing to the log.
     */
    public void logUpdateObject(String collectionName, int id, String jsonObject) throws CommitLogException;

    /**
     * Logging delete object operation in commit log.
     *
     * @param collectionName - name of collection.
     * @param id             - id of object.
     * @throws - if an error occurred when writing to the log.
     */
    public void logDeleteObject(String collectionName, int id) throws CommitLogException;

    /**
     * Method for get number of last commit.
     *
     * @return - last commit number.
     * @throws - if an error occurred when writing to the log.
     */
    public long getLastCommitNumber() throws CommitLogException;

    /**
     * Method for get commits after certain commit.
     *
     * @return - commit number сommits after which will be returned.
     * @throws - if an error occurred when writing to the log.
     */
    public ArrayList<String> getCommitsAfter(long commitNumber) throws CommitLogException;
}
