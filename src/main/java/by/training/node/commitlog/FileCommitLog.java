package by.training.node.commitlog;

import by.training.node.exсeptions.CommitLogException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FileCommitLog implements CommitLog {

    private static final String KEY_DATE = "date";
    private static final String KEY_COMMIT_NUMBER = "commitNumber";
    private static final String KEY_OPERATION = "operation";
    private static final String KEY_COLLECTION = "collection";
    private static final String KEY_OBJECT_ID = "objectId";
    private static final String KEY_JSON_DATA = "jsonData";

    private static final String ERROR_COMMIT_LOG_FILE_NOT_FOUND = "Commit log file not found.";
    private static final String ERROR_IO_EXCEPTION = "Failed or interrupted file operation.";
    private static final String ERROR_CANNOT_PARSE_COMMIT_NUMBER_FROM_STRING = "Cannot parse commit number from" +
            " string in commit log file.";
    private static final String ERROR_CHECK_DELIMITERS = "The commit log file " +
            "structure is broken. Check the correct placement of the delimiters.";
    private static final String ERROR_NEGATIVE_COMMIT_NUMBER = "Negative commit number.";
    private static final String ERROR_INIT_COMMIT_NUMBER = "Cannot initialize commit number";

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(FileCommitLog.class);
    private static final Gson gson = new Gson();

    private String logFileName;
    private DateFormat dateFormat;
    private long commitNumber;


    public FileCommitLog(String logFileName, String dateFormatPattern) {
        this.logFileName = logFileName;
        this.dateFormat = new SimpleDateFormat(dateFormatPattern);
        initCommitId();

    }

    private Map getCommitMapWithDateAndId() throws IOException {
        Map<String, String> commitMap = new HashMap<>();
        String date = dateFormat.format(new Date());
        commitMap.put(KEY_DATE, date);
        commitMap.put(KEY_COMMIT_NUMBER, String.valueOf(commitNumber));
        commitNumber++;
        return commitMap;

    }


    @Override
    public void logCreateCollection(String collectionName, String jsonSchema) throws CommitLogException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            Map<String, String> commitMap = getCommitMapWithDateAndId();
            commitMap.put(KEY_OPERATION, Operation.CREATE_COLLECTION.toString());
            commitMap.put(KEY_COLLECTION, collectionName);
            commitMap.put(KEY_JSON_DATA, jsonSchema);
            String jsonCommit = gson.toJson(commitMap);
            writer.write(jsonCommit);
            writer.newLine();
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }
    }

    @Override
    public void logReadCollection(String collectionName) throws CommitLogException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            Map<String, String> commitMap = getCommitMapWithDateAndId();
            commitMap.put(KEY_OPERATION, Operation.READ_COLLECTION.toString());
            commitMap.put(KEY_COLLECTION, collectionName);
            String jsonCommit = gson.toJson(commitMap);
            writer.write(jsonCommit);
            writer.newLine();
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }
    }

    @Override
    public void logUpdateCollection(String collectionName, String newJsonSchema) throws CommitLogException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            Map<String, String> commitMap = getCommitMapWithDateAndId();
            commitMap.put(KEY_OPERATION, Operation.UPDATE_COLLECTION.toString());
            commitMap.put(KEY_COLLECTION, collectionName);
            commitMap.put(KEY_JSON_DATA, newJsonSchema);
            String jsonCommit = gson.toJson(commitMap);
            writer.write(jsonCommit);
            writer.newLine();
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }
    }

    @Override
    public void logDeleteCollection(String collectionName) throws CommitLogException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            Map<String, String> commitMap = getCommitMapWithDateAndId();
            commitMap.put(KEY_OPERATION, Operation.DELETE_COLLECTION.toString());
            commitMap.put(KEY_COLLECTION, collectionName);
            String jsonCommit = gson.toJson(commitMap);
            writer.write(jsonCommit);
            writer.newLine();
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }

    }

    @Override
    public void logCreateObject(String collectionName, int id, String jsonObject) throws CommitLogException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            Map<String, String> commitMap = getCommitMapWithDateAndId();
            commitMap.put(KEY_OPERATION, Operation.CREATE_OBJECT.toString());
            commitMap.put(KEY_COLLECTION, collectionName);
            commitMap.put(KEY_OBJECT_ID, String.valueOf(id));
            commitMap.put(KEY_JSON_DATA, jsonObject);
            String jsonCommit = gson.toJson(commitMap);
            writer.write(jsonCommit);
            writer.newLine();
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }
    }

    @Override
    public void logReadObject(String collectionName, int id) throws CommitLogException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            Map<String, String> commitMap = getCommitMapWithDateAndId();
            commitMap.put(KEY_OPERATION, Operation.READ_OBJECT.toString());
            commitMap.put(KEY_COLLECTION, collectionName);
            commitMap.put(KEY_OBJECT_ID, String.valueOf(id));
            String jsonCommit = gson.toJson(commitMap);
            writer.write(jsonCommit);
            writer.newLine();
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }
    }

    @Override
    public void logUpdateObject(String collectionName, int id, String jsonObject) throws CommitLogException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            Map<String, String> commitMap = getCommitMapWithDateAndId();
            commitMap.put(KEY_OPERATION, Operation.UPDATE_OBJECT.toString());
            commitMap.put(KEY_COLLECTION, collectionName);
            commitMap.put(KEY_OBJECT_ID, String.valueOf(id));
            commitMap.put(KEY_JSON_DATA, jsonObject);
            String jsonCommit = gson.toJson(commitMap);
            writer.write(jsonCommit);
            writer.newLine();
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }

    }

    @Override
    public void logDeleteObject(String collectionName, int id) throws CommitLogException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(logFileName, true))) {
            Map<String, String> commitMap = getCommitMapWithDateAndId();
            commitMap.put(KEY_OPERATION, Operation.DELETE_OBJECT.toString());
            commitMap.put(KEY_COLLECTION, collectionName);
            commitMap.put(KEY_OBJECT_ID, String.valueOf(id));
            String jsonCommit = gson.toJson(commitMap);
            writer.write(jsonCommit);
            writer.newLine();
            writer.flush();
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }

    }

    @Override
    public long getLastCommitNumber() throws CommitLogException {
        long commitNumber = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(logFileName))) {
            String lastCommitLine = null;
            String currentLine = null;
            while ((currentLine = reader.readLine()) != null) {
                lastCommitLine = currentLine;
            }
            if (lastCommitLine != null) {
                Type type = new TypeToken<Map<String, String>>() {
                }.getType();
                Map<String, String> commitMap = gson.fromJson(lastCommitLine, type);
                commitNumber = Long.parseLong(commitMap.get(KEY_COMMIT_NUMBER));
            } else return 0;

        } catch (NumberFormatException e) {
            throw new CommitLogException(ERROR_CANNOT_PARSE_COMMIT_NUMBER_FROM_STRING, e);
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        } catch (IndexOutOfBoundsException e) {
            throw new CommitLogException(ERROR_CHECK_DELIMITERS, e);
        }
        return commitNumber;
    }

    @Override
    public ArrayList<String> getCommitsAfter(long commitNumber) throws CommitLogException {
        if (commitNumber < 0) {
            throw new CommitLogException(ERROR_NEGATIVE_COMMIT_NUMBER);
        }
        ArrayList<String> commits = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(logFileName))) {
            String line = reader.readLine();
            Type type = new TypeToken<Map<String, String>>() {
            }.getType();
            while (line != null) {
                Map<String, String> commitMap = gson.fromJson(line, type);
                if (Long.parseLong(commitMap.get(KEY_COMMIT_NUMBER)) > commitNumber) {
                    commits.add(line);
                }
                line = reader.readLine();
            }

        } catch (NumberFormatException e) {
            throw new CommitLogException(ERROR_CANNOT_PARSE_COMMIT_NUMBER_FROM_STRING, e);
        } catch (FileNotFoundException e) {
            throw new CommitLogException(ERROR_COMMIT_LOG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            throw new CommitLogException(ERROR_IO_EXCEPTION, e);
        }
        return commits;
    }

    private void initCommitId() {
        try {
            this.commitNumber = getLastCommitNumber();
            this.commitNumber++;
        } catch (CommitLogException e) {
            log.error(ERROR_INIT_COMMIT_NUMBER, e);
        }
    }

}