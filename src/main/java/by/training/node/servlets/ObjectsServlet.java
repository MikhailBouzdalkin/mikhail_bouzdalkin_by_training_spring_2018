package by.training.node.servlets;

import by.training.node.beans.ShardInfo;
import by.training.node.businesslogic.NodeBusinessLogic;
import by.training.node.exсeptions.DAOException;
import by.training.node.exсeptions.WrongInputDataException;
import by.training.node.servlets.logic.JSONLogic;
import by.training.node.servlets.logic.RequestLogic;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet("/app/collection/*")
public class ObjectsServlet extends HttpServlet {
    private static final String KEY_SPRING_CONTEXT = "springContext";
    private static final String KEY_REPLICATIONS = "replications";
    private static final String PATH_DELIMITER = "/";

    private static final String ERROR_JSON_FILE_NOT_FOUND = "JSON file not found.";
    private static final String ERROR_OBJECT_DOES_NOT_EXISTS = "Object does not exists.";
    private static final String ERROR_EMPTY_ID = "Empty id.";

    private static final String REQUEST_PARAMETER_KEY_REPLICATE = "replicate";
    private static final String REQUEST_PARAMETER_VALUE_TRUE = "true";

    private static final int OBJECT_ID_IN_PATH_NUMBER = 2;
    private static final int COLLECTION_NAME_IN_PATH_NUMBER = 1;

    private static final String CONTENT_TYPE_TEXT_PLAIN = "text/plain;charset=UTF-8";

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(ObjectsServlet.class);
    private ApplicationContext springApplicationContext;
    private NodeBusinessLogic nodeBusinessLogic;
    private JSONLogic jsonLogic;
    private RequestLogic requestLogic;
    private List<ShardInfo> replicationsPorts;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String jsonObject = jsonLogic.getJsonFromRequest(req);
        if (jsonObject == null) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_OBJECT_DOES_NOT_EXISTS);
            return;
        }
        String collectionName = getCollectionNameFromRequest(req);
        try {
            int id = nodeBusinessLogic.createObject(collectionName, jsonObject);
            String url = req.getRequestURL().toString();
            resp.setContentType(CONTENT_TYPE_TEXT_PLAIN);
            PrintWriter out = resp.getWriter();
            out.print(id);
            out.flush();
            String parameterReplicate = req.getParameter(REQUEST_PARAMETER_KEY_REPLICATE);
            if ((parameterReplicate == null) || (parameterReplicate.equals(REQUEST_PARAMETER_VALUE_TRUE))) {
                requestLogic.sendRequestToReplications(replicationsPorts, req.getRequestURI(), req.getMethod(),
                        jsonObject);
            }
        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (WrongInputDataException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String jsonObject = jsonLogic.getJsonFromRequest(req);
        if (jsonObject == null) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_JSON_FILE_NOT_FOUND);
            return;
        }
        String collectionName = getCollectionNameFromRequest(req);
        try {
            String objectId = getObjectIdFromRequest(req);
            nodeBusinessLogic.updateObject(collectionName, objectId, jsonObject);
            String parameterReplicate = req.getParameter(REQUEST_PARAMETER_KEY_REPLICATE);
            if ((parameterReplicate == null) || (parameterReplicate.equals(REQUEST_PARAMETER_VALUE_TRUE))) {
                requestLogic.sendRequestToReplications(replicationsPorts, req.getRequestURI(), req.getMethod(),
                        jsonObject);
            }
        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (WrongInputDataException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = getCollectionNameFromRequest(req);

        try {
            String objectId = getObjectIdFromRequest(req);
            nodeBusinessLogic.deleteObject(collectionName, objectId);
            String parameterReplicate = req.getParameter(REQUEST_PARAMETER_KEY_REPLICATE);
            if ((parameterReplicate == null) || (parameterReplicate.equals(REQUEST_PARAMETER_VALUE_TRUE))) {
                requestLogic.sendRequestToReplications(replicationsPorts, req.getRequestURI(), req.getMethod(),
                        null);
            }
        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (WrongInputDataException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = getCollectionNameFromRequest(req);

        try {
            String objectId = getObjectIdFromRequest(req);
            String jsonObject = nodeBusinessLogic.readObjectFromCache(collectionName, objectId);

            if (jsonObject == null) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_OBJECT_DOES_NOT_EXISTS);
            }
            jsonLogic.sendJsonInResponse(resp, jsonObject);
        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (WrongInputDataException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }


    }

    /**
     * Method for get collection name from path.
     *
     * @param req - request with path.
     * @return - name of collection.
     */
    private String getCollectionNameFromRequest(HttpServletRequest req) {
        String[] splittedPath = req.getPathInfo().split(PATH_DELIMITER);
        String collectionName = splittedPath[COLLECTION_NAME_IN_PATH_NUMBER];
        return collectionName;
    }

    /**
     * Method for get object id from path.
     *
     * @param req - request with path.
     * @return - id of object.
     */
    private String getObjectIdFromRequest(HttpServletRequest req) throws WrongInputDataException {

        String[] splittedPath = req.getPathInfo().split(PATH_DELIMITER);
        try {
            String objectId = splittedPath[OBJECT_ID_IN_PATH_NUMBER];
            return objectId;
        } catch (IndexOutOfBoundsException e) {
            throw new WrongInputDataException(ERROR_EMPTY_ID, e);
        }
    }

    @Override
    public void init() throws ServletException {
        ServletContext ctx = getServletContext();
        Logger log = org.apache.logging.log4j.LogManager.getLogger(CollectionsServlet.class);
        springApplicationContext = (ApplicationContext) ctx.getAttribute(KEY_SPRING_CONTEXT);
        nodeBusinessLogic = (NodeBusinessLogic) springApplicationContext.getBean("nodeBusinessLogic");
        jsonLogic = new JSONLogic();
        replicationsPorts = (List<ShardInfo>) ctx.getAttribute(KEY_REPLICATIONS);
        requestLogic = new RequestLogic();
    }
}
