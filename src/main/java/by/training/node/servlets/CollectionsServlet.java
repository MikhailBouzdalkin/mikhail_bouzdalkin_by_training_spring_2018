package by.training.node.servlets;

import by.training.node.beans.ShardInfo;
import by.training.node.businesslogic.NodeBusinessLogic;
import by.training.node.exсeptions.DAOException;
import by.training.node.exсeptions.WrongInputDataException;
import by.training.node.servlets.logic.JSONLogic;
import by.training.node.servlets.logic.RequestLogic;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/app/collections/*")
public class CollectionsServlet extends HttpServlet {
    private static final String ERROR_JSON_FILE_NOT_FOUND = "JSON file not found";

    private static final String KEY_SPRING_CONTEXT = "springContext";
    private static final String KEY_REPLICATIONS = "replications";

    private static final String SPRING_KEY_BUSINESSLOGIC = "nodeBusinessLogic";

    private static final String REQUEST_PARAMETER_KEY_REPLICATE = "replicate";
    private static final String REQUEST_PARAMETER_VALUE_TRUE = "true";

    private static final String PATH_DELIMITER = "/";
    private static final int COLLECTION_NAME_IN_PATH_NUMBER = 1;
    private static final int GET_ALL_COLLECTIONS_NUMBER = 0;

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(CollectionsServlet.class);

    private ApplicationContext springApplicationContext;
    private NodeBusinessLogic nodeBusinessLogic;
    private JSONLogic jsonLogic;
    private RequestLogic requestLogic;
    private List<ShardInfo> replicationsPorts;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String jsonSchemaString = jsonLogic.getJsonFromRequest(req);
        if (jsonSchemaString == null) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_JSON_FILE_NOT_FOUND);
            return;
        }
        String collectionName = getCollectionNameFromRequest(req);
        try {
            nodeBusinessLogic.createCollection(collectionName, jsonSchemaString);
            String parameterReplicate = req.getParameter(REQUEST_PARAMETER_KEY_REPLICATE);
            if ((parameterReplicate == null) || (parameterReplicate.equals(REQUEST_PARAMETER_VALUE_TRUE))) {
                requestLogic.sendRequestToReplications(replicationsPorts, req.getRequestURI(), req.getMethod(),
                        jsonSchemaString);
            }
        } catch (WrongInputDataException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }


    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newJsonSchemaString = jsonLogic.getJsonFromRequest(req);
        if (newJsonSchemaString == null) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_JSON_FILE_NOT_FOUND);
        }
        String collectionName = getCollectionNameFromRequest(req);
        try {
            nodeBusinessLogic.updateCollection(collectionName, newJsonSchemaString);
            String parameterReplicate = req.getParameter(REQUEST_PARAMETER_KEY_REPLICATE);
            if ((parameterReplicate == null) || (parameterReplicate.equals(REQUEST_PARAMETER_VALUE_TRUE))) {
                requestLogic.sendRequestToReplications(replicationsPorts, req.getRequestURI(), req.getMethod(),
                        newJsonSchemaString);
            }

        } catch (WrongInputDataException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }


    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = getCollectionNameFromRequest(req);
        try {
            nodeBusinessLogic.deleteCollection(collectionName);
            String parameterReplicate = req.getParameter(REQUEST_PARAMETER_KEY_REPLICATE);
            if ((parameterReplicate == null) || (parameterReplicate.equals(REQUEST_PARAMETER_VALUE_TRUE))) {
                requestLogic.sendRequestToReplications(replicationsPorts, req.getRequestURI(), req.getMethod(),
                        null);
            }
        } catch (WrongInputDataException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());

        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String collectionName = getCollectionNameFromRequest(req);

        try {
            String resultJsonString = null;
            if (collectionName == null) {
                resultJsonString = nodeBusinessLogic.readAllCollections();
            } else {
                resultJsonString = nodeBusinessLogic.readCollection(collectionName);
            }
            jsonLogic.sendJsonInResponse(resp, resultJsonString);
        } catch (WrongInputDataException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }


    }


    /**
     * Method for get collection name from path.
     *
     * @param req - request with path.
     * @return - name of collection.
     */
    private String getCollectionNameFromRequest(HttpServletRequest req) {
        String[] splittedPath = req.getPathInfo().split(PATH_DELIMITER);
        if (splittedPath.length == GET_ALL_COLLECTIONS_NUMBER) {
            return null;
        }
        String collectionName = splittedPath[COLLECTION_NAME_IN_PATH_NUMBER];
        return collectionName;
    }

    @Override
    public void init() throws ServletException {
        ServletContext ctx = getServletContext();
        Logger log = org.apache.logging.log4j.LogManager.getLogger(CollectionsServlet.class);
        springApplicationContext = (ApplicationContext) ctx.getAttribute(KEY_SPRING_CONTEXT);
        nodeBusinessLogic = (NodeBusinessLogic) springApplicationContext.getBean(SPRING_KEY_BUSINESSLOGIC);
        jsonLogic = new JSONLogic();
        requestLogic = new RequestLogic();
        replicationsPorts = (List<ShardInfo>) ctx.getAttribute(KEY_REPLICATIONS);

    }
}
