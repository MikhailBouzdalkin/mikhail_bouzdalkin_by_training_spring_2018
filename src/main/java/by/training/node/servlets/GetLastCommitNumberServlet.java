package by.training.node.servlets;

import by.training.node.commitlog.CommitLog;
import by.training.node.commitlog.FileCommitLog;
import by.training.node.exсeptions.CommitLogException;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/commitlog/lastcommitnumber")
public class GetLastCommitNumberServlet extends HttpServlet {
    private static final String KEY_SPRING_CONTEXT = "springContext";

    private static final String ERROR_COMMIT_LOG = "Cannot read last commit from commit log.";

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(GetLastCommitNumberServlet.class);
    private ApplicationContext springApplicationContext;
    private CommitLog commitLog;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try (PrintWriter out = resp.getWriter()) {

            out.print(String.valueOf(commitLog.getLastCommitNumber()));
            out.flush();
        } catch (CommitLogException e) {
            log.error(ERROR_COMMIT_LOG, e);
        }
    }

    @Override
    public void init() throws ServletException {
        springApplicationContext = (ApplicationContext) getServletContext()
                .getAttribute(KEY_SPRING_CONTEXT);
        commitLog = (FileCommitLog) springApplicationContext.getBean("fileCommitLog");
    }
}
