package by.training.node.servlets;

import by.training.node.commitlog.CommitLog;
import by.training.node.commitlog.FileCommitLog;
import by.training.node.exсeptions.CommitLogException;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/commitlog/commitsafter")
public class GetCommitsAfterServlet extends HttpServlet {
    private static final String KEY_SPRING_CONTEXT = "springContext";
    private static final String KEY_LAST_COMMIT = "lastcommit";

    private static final String ERROR_PARSE_COMMIT_NUMBER = "Cannot parse commit number form string";
    private static final String ERROR_COMMIT_LOG = "Cannot read last commit from commit log.";

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(GetLastCommitNumberServlet.class);
    private ApplicationContext springApplicationContext;
    private CommitLog commitLog;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try (
                PrintWriter out = resp.getWriter()) {
            long lastCommit = Long.parseLong(req.getParameter(KEY_LAST_COMMIT));
            List<String> commits = commitLog.getCommitsAfter(lastCommit);
            for (String commit : commits) {
                out.println(commit);
            }
            out.flush();
        } catch (CommitLogException e) {
            log.error(ERROR_COMMIT_LOG, e);
        } catch (NumberFormatException e) {
            log.error(ERROR_PARSE_COMMIT_NUMBER, e);
        }
    }

    @Override
    public void init() throws ServletException {
        springApplicationContext = (ApplicationContext) getServletContext()
                .getAttribute(KEY_SPRING_CONTEXT);
        commitLog = (FileCommitLog) springApplicationContext.getBean("fileCommitLog");
    }
}
