package by.training.node.servlets;

import by.training.node.businesslogic.NodeBusinessLogic;
import by.training.node.dao.DAO;
import by.training.node.exсeptions.DAOException;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/healthcheck")
public class HealthCheckServlet extends HttpServlet {
    private static final String KEY_NODE_BUSINESS_LOGIC = "nodeBusinessLogic";
    private static final String KEY_SPRING_CONTEXT = "springContext";
    private static final String TEST_COLLECTION_NAME = "test";
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(HealthCheckServlet.class);
    private ApplicationContext springApplicationContext;
    private DAO dao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            dao.containsCollection(TEST_COLLECTION_NAME);
        } catch (DAOException e) {
            log.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public void init() throws ServletException {
        springApplicationContext = (ApplicationContext) getServletContext()
                .getAttribute(KEY_SPRING_CONTEXT);
        dao = ((NodeBusinessLogic) springApplicationContext.getBean(KEY_NODE_BUSINESS_LOGIC)).getDao();
    }
}
