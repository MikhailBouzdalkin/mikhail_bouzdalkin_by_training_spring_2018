package by.training.node.servlets.logic;

import by.training.node.beans.ShardInfo;
import by.training.node.exсeptions.BadRequestException;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Class used for send request to another shard.
 */
public class RequestLogic {
    private static final String URL_TO_SHARD = "http://%s:%s?replicate=true";
    private static final String ERROR_SEND_REQUEST = "Cannot send request to : ";
    private static final String ERROR_READ_ERROR_RESPONSE_MESSAGE = "Cannot read response message ";
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String REQUEST_METHOD_GET = "GET";
    private static final int RESPONSE_CODE_BAD_REQUEST = 400;

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(RequestLogic.class);

    /**
     * Method for send request to replication of current shard.
     *
     * @param shardInfos - information of replication(host and port).
     * @param path       - path to resource on another shards.
     * @param method     - method of request.
     * @param jsonData   - data in JSON string format.
     */
    public void sendRequestToReplications(List<ShardInfo> shardInfos, String path, String method, String jsonData) {
        for (ShardInfo shardInfo : shardInfos) {
            try {
                sendRequestToShard(String.format(URL_TO_SHARD, shardInfo.getHost(), shardInfo.getPort()
                        + path), method, jsonData);
            } catch (IOException e) {
                log.error(ERROR_SEND_REQUEST + shardInfo, e);
            }
        }

    }

    /**
     * Method for send request to another shard.
     *
     * @param stringURL - URL to resource on another shard.
     * @param method    - method of request.
     * @param jsonData  - data in JSON string format.
     * @return - response body in string format
     * @throws IOException - if error occurs while send or create request.
     */
    public void sendRequestToShard(String stringURL, String method, String jsonData) throws IOException {
        URL url = new URL(stringURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        if ((jsonData != null)) {
            connection.setRequestProperty(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON);
            connection.setDoOutput(true);
            try (OutputStream os = new BufferedOutputStream(connection.getOutputStream())) {
                os.write(jsonData.getBytes());
                os.flush();
            }
        }
        connection.getResponseCode();

    }

    /**
     * Method for send request and get response body in string format.
     *
     * @param stringURL - URL to resource on another shard.
     * @param method    - method of request.
     * @param jsonData  - data in JSON string format.
     * @return - response body in string format
     * @throws IOException         - if error occurs while send
     * @throws BadRequestException - if response returns 400 Bad Request error.
     */
    public String sendRequestAndGetResponseBody(String stringURL, String method, String jsonData) throws IOException,
            BadRequestException {
        URL url = new URL(stringURL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        if (jsonData != null) {
            connection.setRequestProperty(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON);
            connection.setDoOutput(true);
            try (OutputStream os = new BufferedOutputStream(connection.getOutputStream())) {
                os.write(jsonData.getBytes());
                os.flush();
            }
        }
        if (connection.getResponseCode() == RESPONSE_CODE_BAD_REQUEST) {
            throw new BadRequestException();
        } else {
            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()))) {
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                return new String(response);
            }
        }
    }


}
