package by.training.node.servlets.logic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Class for send JSON file or receive it from request.
 */
public class JSONLogic {
    private static final String RESPONSE_CONTENT_TYPE_JSON = "application/json";

    /**
     * Method for get JSON file from request.
     *
     * @param req - request
     *            from which it will be read JSON file.
     * @return - JSON file in String format.
     * @throws IOException - if error occurs while reading file from request.
     */
    public String getJsonFromRequest(HttpServletRequest req) throws IOException {
        StringBuilder buffer = new StringBuilder();
        try (BufferedReader reader = req.getReader()) {
            String jsonFileLine;
            while ((jsonFileLine = reader.readLine()) != null) {
                buffer.append(jsonFileLine);
            }

            String jsonSchemaString = buffer.toString();
            return jsonSchemaString;
        }
    }

    /**
     * Method for send JSON in response.
     *
     * @param resp       - response
     *                   in which will be sent JSON file.
     * @param jsonString - JSON file in String format.
     * @throws IOException - if error occurs while write file to response.
     */
    public void sendJsonInResponse(HttpServletResponse resp, String jsonString) throws IOException {
        resp.setContentType(RESPONSE_CONTENT_TYPE_JSON);
        try (
                PrintWriter out = resp.getWriter()) {
            out.print(jsonString);
            out.flush();
        }
    }
}
