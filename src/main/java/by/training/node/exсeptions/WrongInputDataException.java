package by.training.node.exсeptions;

/**
 * Exception that thrown when using incorrect input data.
 */
public class WrongInputDataException extends Exception {
    public WrongInputDataException(String message) {
        super(message);
    }

    public WrongInputDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
