package by.training.node.exсeptions;


/**
 * Exception which can be thrown when working with DAO.
 */
public class DAOException extends Exception {
    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
