package by.training.node.exсeptions;

/**
 * Exception which can be thrown when working with commit log.
 */
public class CommitLogException extends Exception {
    public CommitLogException(String message) {
        super(message);
    }

    public CommitLogException(String message, Throwable cause) {
        super(message, cause);
    }
}
