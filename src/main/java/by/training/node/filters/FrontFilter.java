package by.training.node.filters;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "frontFilter", urlPatterns = "/app/*")
public class FrontFilter implements Filter {
    private static final String PATH_DELIMITER = "/";
    private static final int ALL_COLLECTION_REQUEST_LENGTH = 4;
    private static final int SINGLE_OBJECT_REQUEST_LENGTH = 5;
    private static final int COLLECTION_WORD_IN_PATH_NUMBER = 2;
    private static final String PATH_COLLECTION = "collection";
    private static final String PATH_COLLECTIONS = "collections";
    private static final String ERROR_CHECK_PATH = "Please check request. For working with collections " +
            "request should start with: 'app/collections/{some collection name}'. " +
            "For working with objects: 'app/collection/{some collection name}/{some object id or empty " +
            "id for add object}'.";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        String path = req.getRequestURI().substring(req.getContextPath().length());
        String[] pathStringArr = path.split(PATH_DELIMITER);
        int pathLength = pathStringArr.length;
        if (path.endsWith(PATH_DELIMITER)) {
            pathLength++;
        }
        if ((pathLength > SINGLE_OBJECT_REQUEST_LENGTH) || (pathLength < ALL_COLLECTION_REQUEST_LENGTH)) {
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        if (((pathLength == ALL_COLLECTION_REQUEST_LENGTH) && (pathStringArr[COLLECTION_WORD_IN_PATH_NUMBER].
                equals(PATH_COLLECTION)))) {
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.
                    SC_BAD_REQUEST, ERROR_CHECK_PATH);
            return;
        }
        if (((pathLength == SINGLE_OBJECT_REQUEST_LENGTH) && (pathStringArr[COLLECTION_WORD_IN_PATH_NUMBER].
                equals(PATH_COLLECTIONS)))) {
            ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_BAD_REQUEST,
                    ERROR_CHECK_PATH);
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }
}
