package by.training.node.filters;

import by.training.node.beans.ShardInfo;
import by.training.node.exсeptions.BadRequestException;
import by.training.node.healthcheck.HealthCheck;
import by.training.node.servlets.logic.JSONLogic;
import by.training.node.servlets.logic.RequestLogic;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

@WebFilter(filterName = "nodeSelectionFilter", urlPatterns = "/app/*")
public class NodeSelectionFilter implements Filter {
    private static final String PATH_DELIMITER = "/";
    private static final int COLLECTION_NAME_IN_PATH_NUMBER = 3;
    private static final int FIRST_ACTIVE_SHARD_NUMBER = 0;
    private static final String CONTEXT_KEY_SPRING_CONTEXT = "springContext";
    private static final String CONTEXT_KEY_CLUSTER = "cluster";
    private static final String CONTEXT_KEY_NODE_ID = "nodeid";
    private static final String URL_REQUEST = "http://%s:%s";
    private static final String REQUEST_METHOD_GET = "GET";
    private static final String ERROR_INACTIVE_NODE = "Node doesn't active.";
    private static final String ERROR_WRITE_RESPONSE = "Cannot write in response body.";
    private static final String ERROR_RESPONSE_RETUNS_WRONG_REQUEST = "Response returns wrong request error.";


    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_JSON = "application/json";

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(NodeSelectionFilter.class);
    private Map<Integer, List<ShardInfo>> cluster;
    private Integer nodeId;
    private RequestLogic requestLogic;
    private JSONLogic jsonLogic;
    private HealthCheck healthCheck;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        servletRequest.getServletContext().getAttribute(CONTEXT_KEY_SPRING_CONTEXT);
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        String path = req.getRequestURI().substring(req.getContextPath().length());
        String collectionName = path.split(PATH_DELIMITER)[COLLECTION_NAME_IN_PATH_NUMBER];
        int nodesNumber = cluster.size();
        int requestedNodeId = collectionName.length() % nodesNumber;
        if (requestedNodeId == nodeId) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        List<ShardInfo> activeShards = healthCheck.getActiveShards(cluster.get(new Integer(requestedNodeId)));
        if (activeShards.size() == 0) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    ERROR_INACTIVE_NODE);
            return;
        }
        String jsonData = jsonLogic.getJsonFromRequest(req);
        ShardInfo shardInfo = activeShards.get(FIRST_ACTIVE_SHARD_NUMBER);
        String responseBody = null;
        try {
            responseBody = requestLogic.sendRequestAndGetResponseBody
                    (String.format(URL_REQUEST, shardInfo.getHost(), shardInfo.getPort() + path),
                            req.getMethod(), jsonData);
        } catch (BadRequestException e) {
            log.error(ERROR_RESPONSE_RETUNS_WRONG_REQUEST, e);
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            return;
        }

        if (req.getMethod().equals(REQUEST_METHOD_GET)) {
            resp.setHeader(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON);
        }
        try (PrintWriter writer = resp.getWriter()) {
            writer.write(responseBody);
            writer.flush();
        } catch (IOException e) {
            log.error(ERROR_WRITE_RESPONSE, e);
        }


    }

    @Override
    public void destroy() {

    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext ctx = filterConfig.getServletContext();
        requestLogic = new RequestLogic();
        cluster = (Map<Integer, List<ShardInfo>>) ctx.getAttribute(CONTEXT_KEY_CLUSTER);
        nodeId = (Integer) ctx.getAttribute(CONTEXT_KEY_NODE_ID);
        jsonLogic = new JSONLogic();
        healthCheck = new HealthCheck();
    }


}
