package by.training.node.healthcheck;

import by.training.node.beans.ShardInfo;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for check state of shards.
 */
public class HealthCheck {
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(HealthCheck.class);
    private static final String ERROR_CANNOT_CONNECT_TO_SHARD = "Cannot connect to shard: ";
    private static final String PATH_TO_SHARD = "http://%s:%s/healthcheck";
    private static final String GET_REQUEST = "GET";
    private static final int RESPONSE_STATUS_OK = 200;
    private static final int TIMEOUT = 100;

    /**
     * Get active shards in node.
     *
     * @param allShards - all shards in node.
     * @return - list of active shards.
     */
    public List<ShardInfo> getActiveShards(List<ShardInfo> allShards) {
        List<ShardInfo> activeShards = new ArrayList<>();
        for (ShardInfo shardInfo : allShards) {
            try {

                URL url = new URL(String.format(PATH_TO_SHARD, shardInfo.getHost(), shardInfo.getPort()));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod(GET_REQUEST);
                connection.setReadTimeout(TIMEOUT);
                if (connection.getResponseCode() == RESPONSE_STATUS_OK) {
                    activeShards.add(shardInfo);
                }
            } catch (IOException e) {
                log.warn(ERROR_CANNOT_CONNECT_TO_SHARD + shardInfo, e);
            }

        }
        return activeShards;
    }

}
