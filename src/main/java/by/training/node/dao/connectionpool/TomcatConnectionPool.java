package by.training.node.dao.connectionpool;

import by.training.node.exсeptions.DAOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class TomcatConnectionPool implements ConnectionPool {
    private static final String INIT_CONTEXT = "java:comp/env";
    private static final String INIT_CONTEXT_KEY_DB = "jdbc/node";
    private static final String ERROR_GET_CONNECTION_FROM_POOL = "Cannot get connection from pool";
    Context envCtx = null;


    public Connection getConnection() throws DAOException {

        try {
            if (envCtx == null) {
                envCtx = (Context) (new InitialContext().lookup(INIT_CONTEXT));
            }
            DataSource ds = (DataSource) envCtx.lookup(INIT_CONTEXT_KEY_DB);
            return ds.getConnection();
        } catch (NamingException | SQLException e) {
            throw new DAOException(ERROR_GET_CONNECTION_FROM_POOL, e);
        }

    }
}
