package by.training.node.dao;

import by.training.node.exсeptions.DAOException;

import java.util.Map;
import java.util.Set;

/*
Interface that represents Data Access Object
 */
public interface DAO {
    /**
     * Method for get Object from DAO.
     *
     * @param collectionName - name of collection which contains the object.
     * @param id             - key associated with object.
     * @return object in JSON string.
     * @throws DAOException - if an error occurred while reading the object from DAO.
     */
    public String getObject(String collectionName, int id) throws DAOException;

    /**
     * Method for put Object in DAO.
     *
     * @param collectionName - name of collection which contains the object.
     * @param jsonObject     - object in JSON string.
     * @throws DAOException - if an error occurred while writing the object in DAO.
     */
    public void putObject(String collectionName, int id, String jsonObject) throws DAOException;

    /**
     * Method for get collection from DAO.
     *
     * @param collectionName - name of collection which contains the objects.
     * @return - map that contains keys( integer value in string format) and associated objects in json string.
     * @throws DAOException - if an error occurred while reading the collection from DAO.
     */
    public Map<String, String> getCollection(String collectionName) throws DAOException;

    /**
     * Method for put collection in DAO.
     *
     * @param collectionName - name of collection which contains the objects.
     * @param jsonSchema     - schema of objects which contains collection.
     * @throws DAOException - if an error occurred while writing the collection in DAO.
     */
    public void putCollection(String collectionName, String jsonSchema) throws DAOException;

    /**
     * Method for get JSON schema in string format of objects contained in the collection from DAO.
     *
     * @param collectionName - name of collection which contains the objects.
     * @return - JSON schema of object in string format.
     * @throws DAOException - if an error occurred while reading the schema from DAO.
     */
    public String getCollectionSchema(String collectionName) throws DAOException;

    /**
     * Method for check does the DAO contains the collection.
     *
     * @param collectionName - name of collection which contains the objects.
     * @return - returns true if this DAO contains the specified collection.
     * @throws DAOException - if an error occurred when using DAO.
     */
    public boolean containsCollection(String collectionName) throws DAOException;

    /**
     * Method for get unique id.
     *
     * @return - unique integer id.
     * @throws - if an error occurred while receiving id from DAO.
     */
    public int getId() throws DAOException;


    /**
     * Method for update object. This method insert new object in json string .
     *
     * @param collectionName - name of collection.
     * @param id             - if of object.
     * @param jsonObject     - new json object.
     * @throws - if an error occurred while updating object in DAO.
     */
    public void updateObject(String collectionName, int id, String jsonObject) throws DAOException;

    /**
     * Method for delete collection from DAO.
     *
     * @param collectionName - name of collection.
     * @throws - if an error occurred while deleting collection from DAO.
     */
    public void deleteCollection(String collectionName) throws DAOException;

    /**
     * Method for delete object from collection.
     *
     * @param id - if of object.
     * @throws - if an error occurred while deleting object from DAO.
     */
    public void deleteObject(String collectionName, int id) throws DAOException;

    /**
     * Method for delete all objects in collection except Json schema.
     *
     * @param collectionName - name of collection which elements will be deleted.
     * @throws - if an error occurred while deleting all objects from DAO.
     */
    public void deleteAllObjectsExceptSchema(String collectionName) throws DAOException;

    /**
     * Method for check if collection contains object in DAO.
     *
     * @param collectionName - name of collection.
     * @param id             - if of object.
     * @throws - if an error occurred while using DAO.
     */
    public boolean containsObject(String collectionName, int id) throws DAOException;

    /**
     * Method for get all collection names.
     *
     * @return set of collection names.
     * @throws - if an error occurred while using DAO.
     */
    public Set<String> getCollectionNames() throws DAOException;

    /**
     * Method for prepare DAO before using.
     */
    public void prepareDAO();
}
