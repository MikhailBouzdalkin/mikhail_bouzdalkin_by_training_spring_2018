package by.training.node.dao;


import by.training.node.dao.connectionpool.ConnectionPool;
import by.training.node.exсeptions.DAOException;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;


public class DatabaseDAO implements DAO {

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(DatabaseDAO.class);
    private static final String ERROR_DATABASE = "Database error.";
    private static final String ERROR_GET_CONNECTION = "Cannot get connection from pool.";
    private static final String ERROR_SQL = "Unable to execute SQL query.";
    private static final String ERROR_CONFIG_FILE_NOT_FOUND = "Functions file not found.";
    private static final String ERROR_READ_CONFIG_FILE = "Error while reading properties file.";
    private static final String SQL_GET_ID = "SELECT nextval('Id');";
    private static final String SQL_CONTAINS_FUNCTION = "select exists(select * from pg_proc where proname = ?);";
    private static final String SQL_CONTAINS_COLLECTION = " SELECT to_regclass((?)::cstring);";
    private static final String SQL_GET_ALL_COLLECTIONS = "SELECT table_name FROM information_schema.tables" +
            "                      WHERE table_schema='public';";
    private static final String SQL_CS_GET_OBJECT = "{? = call get_object(?,?)}";
    private static final String SQL_CS_PUT_OBJECT = "{call insert_object(?,?,?)}";
    private static final String SQL_PS_GET_COLLECTION = "SELECT * FROM get_collection(?);";
    private static final String SQL_CS_CREATE_COLLECTION = "{call create_collection(?)}";
    private static final String SQL_CS_INSERT_SCHEMA = "{call insert_schema(?,?)}";
    private static final String SQL_CS_GET_SCHEMA = "{? = call get_schema(?)}";
    private static final String SQL_CS_UPDATE_OBJECT = "{call update_object(?,?,?)}";
    private static final String SQL_CS_DELETE_COLLECTION = "{call drop_collection(?)}";
    private static final String SQL_CS_DELETE_OBJECT = "{call delete_object(?,?)}";
    private static final String SQL_CS_DELETE_OBJECTS_EXCEPT_SCHEMA = "{call delete_objects_except_schema(?)}";
    private static final String SQL_CS_CONTAINS_OBJECT = "{?=call check_object_exists(?,?)}";
    private static ConnectionPool connectionPool;
    private String functionsFile;

    public DatabaseDAO(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    public void setFunctionsFile(String functionsFile) {
        this.functionsFile = functionsFile;
    }


    @Override
    public String getObject(String collectionName, int id) throws DAOException {
        String resultObject = null;
        try (Connection cn = connectionPool.getConnection();
             CallableStatement cs = cn.prepareCall(SQL_CS_GET_OBJECT)) {
            cs.setString(2, collectionName);
            cs.setInt(3, id);
            cs.registerOutParameter(1, Types.LONGVARCHAR);
            cs.execute();
            resultObject = cs.getString(1);
            return resultObject;

        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }

    @Override
    public void putObject(String collectionName, int id, String jsonObject) throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             CallableStatement cs = cn.prepareCall(SQL_CS_PUT_OBJECT)) {
            cs.setString(1, collectionName);
            cs.setInt(2, id);
            cs.setString(3, jsonObject);
            cs.execute();
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }


    @Override
    public Map<String, String> getCollection(String collectionName) throws DAOException {
        Map<String, String> resultMap = new HashMap<>();
        try (Connection cn = connectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(SQL_PS_GET_COLLECTION)) {
            ps.setString(1, collectionName);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    resultMap.put(String.valueOf(resultSet.getInt(1)),
                            resultSet.getString(2));
                }
                return resultMap;
            }
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }


    @Override
    public void putCollection(String collectionName, String jsonSchema) throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             CallableStatement csCreateCollection = cn.prepareCall(SQL_CS_CREATE_COLLECTION);
             CallableStatement csInsertSchema = cn.prepareCall(SQL_CS_INSERT_SCHEMA)) {
            csCreateCollection.setString(1, collectionName);
            csCreateCollection.execute();
            csInsertSchema.setString(1, collectionName);
            csInsertSchema.setString(2, jsonSchema);
            csInsertSchema.execute();
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }


    @Override
    public String getCollectionSchema(String collectionName) throws DAOException {
        String schema = null;
        try (Connection cn = connectionPool.getConnection();
             CallableStatement cs = cn.prepareCall(SQL_CS_GET_SCHEMA)) {
            cs.setString(2, collectionName);
            cs.registerOutParameter(1, Types.LONGVARCHAR);
            cs.execute();
            schema = cs.getString(1);
            return schema;

        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }


    @Override
    public boolean containsCollection(String collectionName) throws DAOException {
        boolean contains = false;
        try (Connection cn = connectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(SQL_CONTAINS_COLLECTION)) {
            ps.setString(1, collectionName);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    rs.getObject(1);
                    if (!(rs.wasNull())) {
                        contains = true;
                    }
                }
                return contains;
            }

        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }


    }

    @Override
    public int getId() throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             Statement statement = cn.createStatement();
             ResultSet resultSetId = statement.executeQuery(SQL_GET_ID);) {
            int id = 0;
            if (resultSetId.next()) {
                id = resultSetId.getInt(1);
            }
            return id;
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }

    }

    @Override
    public void updateObject(String collectionName, int id, String jsonObject) throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             CallableStatement cs = cn.prepareCall(SQL_CS_UPDATE_OBJECT)) {
            cs.setString(1, collectionName);
            cs.setInt(2, id);
            cs.setString(3, jsonObject);
            cs.execute();
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }


    @Override
    public void deleteCollection(String collectionName) throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             CallableStatement cs = cn.prepareCall(SQL_CS_DELETE_COLLECTION)) {
            cs.setString(1, collectionName);
            cs.execute();
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }


    @Override
    public void deleteObject(String collectionName, int id) throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             CallableStatement cs = cn.prepareCall(SQL_CS_DELETE_OBJECT)) {
            cs.setString(1, collectionName);
            cs.setInt(2, id);
            cs.execute();
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }


    @Override
    public void deleteAllObjectsExceptSchema(String collectionName) throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             CallableStatement cs = cn.prepareCall(SQL_CS_DELETE_OBJECTS_EXCEPT_SCHEMA)) {
            cs.setString(1, collectionName);
            cs.execute();
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }


    @Override
    public boolean containsObject(String collectionName, int id) throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             CallableStatement cs = cn.prepareCall(SQL_CS_CONTAINS_OBJECT)) {
            cs.setString(2, collectionName);
            cs.setInt(3, id);
            cs.registerOutParameter(1, Types.BOOLEAN);
            cs.execute();
            return cs.getBoolean(1);
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }
    }

    @Override
    public Set<String> getCollectionNames() throws DAOException {
        try (Connection cn = connectionPool.getConnection();
             Statement statement = cn.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_GET_ALL_COLLECTIONS);) {
            Set<String> namesSet = new HashSet<>();
            while (resultSet.next()) {
                namesSet.add(resultSet.getString(1));
            }
            return namesSet;
        } catch (SQLException e) {
            throw new DAOException(ERROR_DATABASE, e);
        }


    }

    @Override
    public void prepareDAO() {
        try (InputStream input = new FileInputStream(functionsFile)) {
            Properties prop = new Properties();
            prop.load(input);
            Enumeration<String> propertyNames = (Enumeration<String>) prop.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String function_name = propertyNames.nextElement();
                try (Connection cn = connectionPool.getConnection();
                     PreparedStatement psContainsCollection = cn.prepareStatement(SQL_CONTAINS_FUNCTION)) {
                    psContainsCollection.setString(1, function_name);
                    try (ResultSet rs = psContainsCollection.executeQuery()) {
                        if (rs.next()) {
                            boolean contains = rs.getBoolean(1);
                            if (contains == false) {
                                try (PreparedStatement psCreateFunction =
                                             cn.prepareStatement(prop.getProperty(function_name))) {
                                    psCreateFunction.executeUpdate();
                                }
                            }
                        }

                    }

                } catch (SQLException e) {
                    log.error(ERROR_SQL, e);
                } catch (DAOException e) {
                    log.error(ERROR_GET_CONNECTION, e);
                }
            }
        } catch (FileNotFoundException e) {
            log.error(ERROR_CONFIG_FILE_NOT_FOUND, e);
        } catch (IOException e) {
            log.error(ERROR_READ_CONFIG_FILE, e);
        }
    }


}
