package by.training.elevator.elevator;

import by.training.elevator.exceptions.IncorrectConfigurationValueException;
import by.training.elevator.passengers.Passenger;
import by.training.elevator.passengers.TransportationState;
import by.training.elevator.passengers.TransportationTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.Phaser;



/**
 * Сlass is an elevator that moves passengers in the building.
 */
public class Elevator {
    private static final String CONFIG_FILE = "/config.properties";

    private static final String ERROR_INCORRECT_STORIES_NUMBER_VALUE = "Incorrect property value: storiesNumber less than 2.";
    private static final String ERROR_INCORRECT_ELEVATOR_CAPACITY_VALUE = "Incorrect property value: elevatorCapacity less than 1.";
    private static final String ERROR_INCORRECT_PASSENGERS_NUMBER_VALUE = "Incorrect property value: storiesNumber less than 0.";
    private static final String PROPERTY_KEY_STORIES_NUMBER = "storiesNumber";
    private static final String PROPERTY_KEY_ELEVATOR_CAPACITY = "elevatorCapacity";
    private static final String PROPERTY_KEY_PASSENGERS_NUMBER = "passengersNumber";

    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(Elevator.class);

    private int storiesNumber;
    private int elevatorCapacity;
    private int passengersNumber;
    /**
     * Array of all stories in the building.
     */
    private Story[] stories;
    /**
     * Array of all passengers in the building.
     */
    private Passenger[] passengers;
    private List<Passenger> evelvatorContainer;


    public Elevator() throws IOException, IncorrectConfigurationValueException {
        InputStream in = Elevator.class.getResourceAsStream(CONFIG_FILE);
        Properties configProps = new Properties();

        configProps.load(in);
        storiesNumber = Integer.parseInt(configProps.getProperty(PROPERTY_KEY_STORIES_NUMBER));
        elevatorCapacity = Integer.parseInt(configProps.getProperty(PROPERTY_KEY_ELEVATOR_CAPACITY));
        passengersNumber = Integer.parseInt(configProps.getProperty(PROPERTY_KEY_PASSENGERS_NUMBER));
        if (storiesNumber < 2) {
            throw new IncorrectConfigurationValueException(ERROR_INCORRECT_STORIES_NUMBER_VALUE);
        }
        if (elevatorCapacity < 1) {
            throw new IncorrectConfigurationValueException(ERROR_INCORRECT_ELEVATOR_CAPACITY_VALUE);
        }
        if (passengersNumber < 0) {
            throw new IncorrectConfigurationValueException(ERROR_INCORRECT_PASSENGERS_NUMBER_VALUE);
        }
        stories = new Story[storiesNumber];
        passengers = new Passenger[passengersNumber];
        for (int i = 0; i < storiesNumber; i++) {
            stories[i] = new Story(i);
        }
        for (int i = 0; i < passengersNumber; i++) {
            int initialStory = i % storiesNumber;
            Passenger passenger = new Passenger(i, initialStory, randomDestinationStory(initialStory, storiesNumber));
            passengers[i] = passenger;
            stories[initialStory].getDispatchStoryContainer().add(passenger);
        }
        evelvatorContainer = new LinkedList<>();

    }

    /**
     * Method for get destination story different from the initial
     *
     * @param initialStory  - initial story of passenger
     * @param storiesNumber - number of stories in the building
     * @return destination story
     */
    private static int randomDestinationStory(int initialStory, int storiesNumber) {
        int destinationStory;
        do {
            Random random = new Random();
            destinationStory = random.nextInt(storiesNumber);
        } while (destinationStory == initialStory);
        return destinationStory;
    }

    /**
     * Inner class that represents controller that controls the elevator.
     */
    public static class Controller {
        private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(Elevator.Controller.class);

        private static final String LOG_STARTING_TRANSPORTATION = "STARTING_TRANSPORTATION";
        private static final String LOG_COMPLETION_TRANSPORTATION = "COMPLETION_TRANSPORTATION";
        private static final String LOG_MOVING_ELEVATOR = "MOVING_ELEVATOR (from story- {} to story- {})";
        private static final String LOG_BOADING_ON_PASSENGER = "BOADING_ON_PASSENGER ({} on story- {})";
        private static final String LOG_DEBOADING_ON_PASSENGER = "DEBOADING_PASSENGER ({} on story-{})";
        private static final String LOG_VALIDATION_SUCCESSFUL = "VALIDATION COMPLETED SUCCESSFUL";
        private static final String LOG_VALIDATION_ERROR = "ERRORS IN VALIDATION: ";

        private static final String ERROR_FILE_NOT_FOUND_MESSAGE = "File not found";


        private Elevator elevator;
        /**
         * Number of transported passengers(
         * used to know when to terminate a transportation).
         */
        private int transportedPassengersNumber = 0;
        /**
         * Phaser used to stop the main thread when boarding and disembarking passengers.
         * Initialized with 1 for main Thread.
         */
        private Phaser phaser = new Phaser(1);
        ;
        int currentStoryNumber = 1;
        /**
         * The value on which the currentStoryNumber is incremented when moving the elevator.
         * If the elevator goes up, then it is positive, if down negative.
         */
        int delta = 1;

        public Elevator getElevator() {
            return elevator;
        }

        /**
         * Method for running the elevator.
         */
        public void run() {
            try {
                elevator = new Elevator();
                int passengersNumber = elevator.passengersNumber;
                log.info(LOG_STARTING_TRANSPORTATION);
                createTransportationTasks();
                while (transportedPassengersNumber < passengersNumber) {
                    if ((currentStoryNumber == elevator.storiesNumber - 1) || (currentStoryNumber == 0)) {
                        delta = -delta;
                    }
                    notifyPassengersToGetOutOfElevator();
                    phaser.arriveAndAwaitAdvance();
                    notifyPassengersToGoIntoElevator(elevator.stories[currentStoryNumber].getDispatchStoryContainer());
                    phaser.arriveAndAwaitAdvance();
                    log.info(LOG_MOVING_ELEVATOR, String.valueOf(currentStoryNumber),
                            String.valueOf(currentStoryNumber + delta));
                    currentStoryNumber += delta;
                }
                log.info(validateCompletionTransportation());
                log.info(LOG_COMPLETION_TRANSPORTATION);
            } catch (IncorrectConfigurationValueException e) {
                log.error(e);
            } catch (IOException e) {
                log.error(ERROR_FILE_NOT_FOUND_MESSAGE, e);
            }
        }

        /**
         * Method to notify Transportation Tasks of passengers to exit the elevator.
         */
        private synchronized void notifyPassengersToGetOutOfElevator() {

            for (Passenger passenger : elevator.evelvatorContainer) {
                if (passenger.getDestinationStory() == currentStoryNumber) {
                    phaser.register();
                    passenger.getTransportationTask().notifyTask();
                }
            }

        }

        /**
         * Method to exit the passenger from the elevator.
         *
         * @param passenger - passenger who exits the elevator.
         */
        public synchronized void getOutOfElevator(Passenger passenger) {
            List<Passenger> arrivalStoryContainer =
                    elevator.stories[passenger.getDestinationStory()].getArrivalStoryContainer();
            elevator.evelvatorContainer.remove(passenger);
            arrivalStoryContainer.add(passenger);
            log.info(LOG_DEBOADING_ON_PASSENGER, passenger.getPassengerID(), passenger.getDestinationStory());
            transportedPassengersNumber++;
            phaser.arriveAndDeregister();

        }

        /**
         * Method to notify Transportation Tasks of passengers to go into the elevator.
         *
         * @param currentStoryDispatchContainer - Dispatch container of current story.
         */
        private synchronized void notifyPassengersToGoIntoElevator(List<Passenger> currentStoryDispatchContainer) {
            int freePositions = elevator.elevatorCapacity - elevator.evelvatorContainer.size();
            for (Passenger passenger : currentStoryDispatchContainer) {
                if ((delta * (passenger.getDestinationStory() - passenger.getInitialStory()) > 0) && (freePositions > 0)) {
                    phaser.register();
                    passenger.getTransportationTask().notifyTask();
                    freePositions--;
                }
            }
        }

        /**
         * Method to enter the passenger in the elevator.
         *
         * @param passenger - passenger who enter the elevator.
         */
        public synchronized void getIntoElevator(Passenger passenger) {

            int currentcurrentStoryNumber = passenger.getInitialStory();
            List<Passenger> dispatchStoryContainer = elevator.stories[currentcurrentStoryNumber].getDispatchStoryContainer();
            dispatchStoryContainer.remove(passenger);
            elevator.evelvatorContainer.add(passenger);
            log.info(LOG_BOADING_ON_PASSENGER, passenger.getPassengerID(), passenger.getInitialStory());
            phaser.arriveAndDeregister();

        }

        /**
         * Method for creating transportation tasks for each passenger.
         */
        private void createTransportationTasks() {
            int passengersNumber = elevator.passengersNumber;
            Passenger[] passengers = elevator.passengers;
            for (int i = 0; i < passengersNumber; i++) {
                TransportationTask transportationTask = new TransportationTask(elevator.passengers[i], this);
                passengers[i].setTransportationTask
                        (transportationTask);
                transportationTask.start();

            }

        }

        private String validateCompletionTransportation() {
            boolean result = true;
            result = result && elevator.evelvatorContainer.size() == 0;
            int arrivalStoryContainerPassengersNumber = 0;
            for (Story story : elevator.stories) {
                result = result && story.getDispatchStoryContainer().size() == 0;
                for (Passenger passenger : story.getArrivalStoryContainer()) {
                    result = result && passenger.
                            getTransportationState() == TransportationState.COMPLETED;
                    result = result && passenger.getDestinationStory() == story.getNumber();
                    arrivalStoryContainerPassengersNumber++;
                }
            }
            result = result &&
                    arrivalStoryContainerPassengersNumber == elevator.passengersNumber;
            if (result) {
                return LOG_VALIDATION_SUCCESSFUL;
            } else {
                return LOG_VALIDATION_ERROR ;
            }

        }

    }

}
