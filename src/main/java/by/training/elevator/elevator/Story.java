package by.training.elevator.elevator;



import by.training.elevator.passengers.Passenger;

import java.util.LinkedList;
import java.util.List;

/**
 * Сlass representing the story of a building.
 */
public class Story {
    /**
     * Number of the story.
     */
    private int number;
    /**
     * Container for passengers which need to be transported.
     */
 private List<Passenger> dispatchStoryContainer;
    /**
     * Container for transported passengers.
     */
 private List<Passenger> arrivalStoryContainer;


    /**
     * Constructor.
     * @param number - Number of the story.
     */
    public Story(int number) {
        arrivalStoryContainer = new LinkedList<>();
        dispatchStoryContainer= new LinkedList<>();
        this.number=number;

    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Passenger> getDispatchStoryContainer() {
        return dispatchStoryContainer;
    }

    public void setDispatchStoryContainer(List<Passenger> dispatchStoryContainer) {
        this.dispatchStoryContainer = dispatchStoryContainer;
    }

    public List<Passenger> getArrivalStoryContainer() {
        return arrivalStoryContainer;
    }

    public void setArrivalStoryContainer(List<Passenger> arrivalStoryContainer) {
        this.arrivalStoryContainer = arrivalStoryContainer;
    }
}
