package by.training.elevator.passengers;


import by.training.elevator.elevator.Elevator;

public class TransportationTask extends Thread {
    /**
     * by.training.passengers.Passenger with whom the object of by.training.passengers.TransportationTask is associated.
     */
    private Passenger passenger;
    /**
     * Controller of elevator.
     */
    private Elevator.Controller controller;
    private boolean notified=false;

    /**
     * Constructor.
     * @param passenger -  passenger with whom the object of by.training.passengers.TransportationTask is associated.
     * @param controller - controller of elevator.
     */
    public TransportationTask(Passenger passenger, Elevator.Controller controller) {
        this.passenger = passenger;
        this.controller = controller;
        passenger.setTransportationState(TransportationState.IN_PROGRESS);

    }

    /**
     * Constructor.
     */
    public TransportationTask() {
    }

    @Override
    public void run() {
        waitTask();
        controller.getIntoElevator(passenger);
        notified=false;
        waitTask();
        controller.getOutOfElevator(passenger);
        passenger.setTransportationState(TransportationState.COMPLETED);
    }

    /**
     * Method for waiting by.training.passengers.TransportationTask thread.
     */
    private void waitTask(){
        synchronized (this) {
            try {
                while (!notified){
                    wait();}
            } catch (InterruptedException e) {
                passenger.setTransportationState(TransportationState.ABORTED);
            }
        }
    }
    /**
     * Method for notified by.training.passengers.TransportationTask thread.
     */
    public void notifyTask() {
        synchronized (this) {
                notified=true;
                notify();
        }
    }
}
