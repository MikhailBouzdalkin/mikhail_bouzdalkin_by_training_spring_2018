package by.training.elevator.passengers;

/**
 * Enum thad represents state of passenger's transportation.
 */
public enum TransportationState {
    NOT_STARTED,IN_PROGRESS,COMPLETED,ABORTED;
}
