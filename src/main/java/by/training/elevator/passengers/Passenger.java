package by.training.elevator.passengers;

public class Passenger {

    private TransportationState transportationState;
    private int passengerID;
    private int initialStory;
    private int destinationStory;
    private TransportationTask transportationTask;


    /**
     * Constructor.
     * @param passengerID - ID of passenger.
     * @param initialStory - the story at which the passenger enters the elevator.
     * @param destinationStory - the story at which the passenger exit the elevator.
     */
    public Passenger(int passengerID, int initialStory, int destinationStory) {

        this.passengerID = passengerID;
        this.initialStory = initialStory;
        this.destinationStory = destinationStory;
        this.transportationState=TransportationState.NOT_STARTED;

    }

    public int getPassengerID() {
        return passengerID;
    }

    public void setPassengerID(int passengerID) {
        this.passengerID = passengerID;
    }

    public int getInitialStory() {
        return initialStory;
    }

    public void setInitialStory(int initialStory) {
        this.initialStory = initialStory;
    }

    public int getDestinationStory() {
        return destinationStory;
    }

    public void setDestinationStory(int destinationStory) {
        this.destinationStory = destinationStory;
    }

    public TransportationState getTransportationState() {
        return transportationState;
    }

    public void setTransportationState(TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    public TransportationTask getTransportationTask() {
        return transportationTask;
    }

    public void setTransportationTask(TransportationTask transportationTask) {
        this.transportationTask = transportationTask;
    }
}
