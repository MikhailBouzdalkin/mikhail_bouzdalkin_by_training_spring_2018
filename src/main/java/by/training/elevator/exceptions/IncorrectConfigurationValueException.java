package by.training.elevator.exceptions;

public class IncorrectConfigurationValueException extends Exception{
    public IncorrectConfigurationValueException() {
    }

    public IncorrectConfigurationValueException(String message) {
        super(message);
    }
}
