package by.training.tags;

import javax.servlet.ServletRegistration;
import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.IterationTag;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Iterator;
import java.util.Map;

/**
 * The tag that is used to list all the urls mapping in the deployed application.
 */
public class ListMappingTag extends TagSupport {
    private static final String ATTRIBUTE_NAME_URL="url";
    private static final String ATTRIBUTE_NAME_SERVLET="servlet";

    private Map<String, ? extends ServletRegistration> servletRegistrarions;
    private Iterator<? extends Map.Entry<String, ? extends ServletRegistration>> iterator;
    private ServletRequest request;

    @Override
    public int doAfterBody() throws JspException {
        if (this.iterator.hasNext()) {
            final Map.Entry<String, ? extends ServletRegistration> entry = this.iterator.next();
            this.request.setAttribute(ATTRIBUTE_NAME_URL, entry.getValue().getMappings());
            this.request.setAttribute(ATTRIBUTE_NAME_SERVLET, entry.getKey());
        }
        if (this.iterator.hasNext()) {
            return IterationTag.EVAL_BODY_AGAIN;
        } else {
            return Tag.SKIP_BODY;
        }

    }

    @Override
    public int doStartTag() throws JspException {
        this.servletRegistrarions = this.pageContext.getServletContext().getServletRegistrations();
        this.iterator = this.servletRegistrarions.entrySet().iterator();
        this.request = this.pageContext.getRequest();
        final Map.Entry<String, ? extends ServletRegistration> entry = this.iterator.next();
        this.request.setAttribute(ATTRIBUTE_NAME_URL, entry.getValue().getMappings());
        this.request.setAttribute(ATTRIBUTE_NAME_SERVLET, entry.getKey());
        return Tag.EVAL_BODY_INCLUDE;
    }

}
