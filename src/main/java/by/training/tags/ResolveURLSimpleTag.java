package by.training.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Tag that is used to resolve appropriate web component (accordingly to the mapping) for the posted
 * url.
 */
public class ResolveURLSimpleTag extends SimpleTagSupport {
    /**
     * Tag attribute - url for which will resolve web component
     */
    private String url;

    @Override
    public void doTag() throws JspException, IOException {
        final PageContext pageContext = (PageContext) this.getJspContext();
        pageContext.getRequest().setAttribute("url", this.url);
        pageContext.getRequest().setAttribute("resource",
                pageContext.getServletContext().getRealPath(this.url));
        this.getJspBody().invoke(null);
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
