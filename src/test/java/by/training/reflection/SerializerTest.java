package by.training.reflection;


import by.training.reflection.annotations.Serializable;
import by.training.reflection.serializers.Serializer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.Objects;

public class SerializerTest {
    @Serializable()
    public static class Student{
        @Serializable(alias = "n")
        String name;
        @Serializable(alias = "s")
        String surname;
        int age;

        public Student(String name, String surname, int age) {
            this.name = name;
            this.surname = surname;
            this.age = age;
        }

        public Student() {
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Student student = (Student) o;
            return Objects.equals(name, student.name) &&
                    Objects.equals(surname, student.surname);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, surname);
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    @Test
    public void TestSerializeAndDeserialize() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        Student stud0= new Student("Ivan","Ivanov",21);
        Map<String,String> map=Serializer.serialize(stud0);
        Student stud1= (Student) Serializer.deserialize(map);
        Assert.assertEquals(stud0,stud1);

    }

}
