package by.training.reflection;


import by.training.reflection.annotations.Proxy;
import by.training.reflection.factories.Factory;
import org.junit.Assert;
import org.junit.Test;

import java.util.logging.Logger;


public class ProxyTest {
    private static Logger log = Logger.getLogger(ProxyTest.class.getName());
    public static class Duck{
       void speak(){
       log.info("KRYA");
       }
    }
    @Proxy(invocationHandler = "by.training.invocationhandlers.SomeInvocationHandler")
    public class Cow{
        void speak(){
            log.info("MU");
        }
    }
    @Proxy(invocationHandler = "NotExist")
    public static class Dog{
        void speak(){
            log.info("GAV");
        }
    }
    @Test
    public void testDuck() {
        Assert.assertFalse(Factory.getInstanceOf(Duck.class) instanceof java.lang.reflect.Proxy);
    }
    @Test
    public void testCow() {
        Assert.assertTrue(Factory.getInstanceOf(Cow.class) instanceof java.lang.reflect.Proxy);
    }
    @Test
    public void testDog() {
        Assert.assertNull(Factory.getInstanceOf(Duck.class));
    }

}
