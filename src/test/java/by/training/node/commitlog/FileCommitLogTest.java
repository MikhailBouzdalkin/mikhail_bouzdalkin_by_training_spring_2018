package by.training.node.commitlog;

import by.training.node.exсeptions.CommitLogException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FileCommitLogTest {
    private static final String KEY_DATE="date";
    private static final String KEY_COMMIT_NUMBER="commitNumber";
    private static final String KEY_OPERATION="operation";
    private static final String KEY_COLLECTION="collection";
    private static final String KEY_OBJECT_ID="objectId";
    private static final String KEY_JSON_DATA="jsonData";
    private static final String EMPTY_FIELD = "";
    private static String fileName = "test.txt";
    private static File file = new File(fileName);
    private static FileCommitLog log ;
    private String collectionName = "testCollectionName";
    private String collectionSchema = "schema";
    private String object = "object";
    private int objectId = 1;

    private static final Gson gson =new Gson();

    @Test
    public void testLogCreateCollection() throws CommitLogException, IOException {
        log.logCreateCollection(collectionName, collectionSchema);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,EMPTY_FIELD);
        expectedMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
        expectedMap.put(KEY_OPERATION,Operation.CREATE_COLLECTION.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.put(KEY_JSON_DATA,collectionSchema);
        Assert.assertEquals(expectedMap,getLastCommitFieldsWithEmptyDateAndCommitNumber());
    }

    @Test
    public void testLogReadCollection() throws CommitLogException, IOException {
        log.logReadCollection(collectionName);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,EMPTY_FIELD);
        expectedMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
        expectedMap.put(KEY_OPERATION,Operation.READ_COLLECTION.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        Assert.assertEquals(expectedMap,getLastCommitFieldsWithEmptyDateAndCommitNumber());

    }

    @Test
    public void testLogUpdateCollection() throws CommitLogException, IOException {
        String updatedSchema = "updatedSchema";
        log.logUpdateCollection(collectionName, updatedSchema);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,EMPTY_FIELD);
        expectedMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
        expectedMap.put(KEY_OPERATION,Operation.UPDATE_COLLECTION.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.put(KEY_JSON_DATA,updatedSchema);
        Assert.assertEquals(expectedMap,getLastCommitFieldsWithEmptyDateAndCommitNumber());
    }

    @Test
    public void testLogDeleteCollection() throws CommitLogException, IOException {
        log.logDeleteCollection(collectionName);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,EMPTY_FIELD);
        expectedMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
        expectedMap.put(KEY_OPERATION,Operation.DELETE_COLLECTION.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        Assert.assertEquals(expectedMap,getLastCommitFieldsWithEmptyDateAndCommitNumber());

    }

    @Test
    public void testLogCreateObject() throws CommitLogException, IOException {
        log.logCreateObject(collectionName, objectId, object);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,EMPTY_FIELD);
        expectedMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
        expectedMap.put(KEY_OPERATION,Operation.CREATE_OBJECT.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.put(KEY_OBJECT_ID,String.valueOf(objectId));
        expectedMap.put(KEY_JSON_DATA,object);
        Assert.assertEquals(expectedMap,getLastCommitFieldsWithEmptyDateAndCommitNumber());

    }

    @Test
    public void testLogReadObject() throws CommitLogException, IOException {
        log.logReadObject(collectionName, objectId);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,EMPTY_FIELD);
        expectedMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
        expectedMap.put(KEY_OPERATION,Operation.READ_OBJECT.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.put(KEY_OBJECT_ID,String.valueOf(objectId));
        Assert.assertEquals(expectedMap,getLastCommitFieldsWithEmptyDateAndCommitNumber());

    }

    @Test
    public void testLogUpdateObject() throws CommitLogException, IOException {
        String updatedObject = "updatedObject";
        log.logUpdateObject(collectionName, objectId, updatedObject);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,EMPTY_FIELD);
        expectedMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
        expectedMap.put(KEY_OPERATION,Operation.UPDATE_OBJECT.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.put(KEY_OBJECT_ID,String.valueOf(objectId));
        expectedMap.put(KEY_JSON_DATA,updatedObject);
        Assert.assertEquals(expectedMap,getLastCommitFieldsWithEmptyDateAndCommitNumber());

    }

    @Test
    public void testLogDeleteObject() throws CommitLogException, IOException {
        log.logDeleteObject(collectionName, objectId);
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,EMPTY_FIELD);
        expectedMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
        expectedMap.put(KEY_OPERATION,Operation.DELETE_OBJECT.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.put(KEY_OBJECT_ID,String.valueOf(objectId));
        Assert.assertEquals(expectedMap,getLastCommitFieldsWithEmptyDateAndCommitNumber());
    }

    @Test
    public void testLastCommitNumber() throws IOException, CommitLogException {
        long commitNumber = 0;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            Map<String,String> expectedMap = new HashMap<>();
            expectedMap.put(KEY_DATE,"date");
            expectedMap.put(KEY_COMMIT_NUMBER,String.valueOf(commitNumber));
            expectedMap.put(KEY_OPERATION,Operation.UPDATE_OBJECT.toString());
            expectedMap.put(KEY_COLLECTION,collectionName);
            expectedMap.put(KEY_OBJECT_ID,String.valueOf(objectId));
            expectedMap.put(KEY_JSON_DATA,object);
            writer.write(gson.toJson(expectedMap));
            writer.newLine();
        }
        Assert.assertEquals(commitNumber, log.getLastCommitNumber());
    }

    @Test
    public void testGetCommitsAfter() throws IOException, CommitLogException {
        String fileName = "testGetCommit.txt";
        File file = new File(fileName);
        file.createNewFile();
        FileCommitLog log = new FileCommitLog(fileName, "yyyy.MM.dd 'at' HH:mm:ss z");
        long commitNumber = 0;
        String date = "date";
        String updatedObject="updatedObject";
        String collectionSchema="collectionSchema";
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put(KEY_DATE,date);
        expectedMap.put(KEY_COMMIT_NUMBER,String.valueOf(commitNumber++));
        expectedMap.put(KEY_OPERATION,Operation.UPDATE_OBJECT.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.put(KEY_OBJECT_ID,String.valueOf(objectId));
        expectedMap.put(KEY_JSON_DATA,updatedObject);
        String commit0=gson.toJson(expectedMap);
        expectedMap.put(KEY_DATE,date);
        expectedMap.put(KEY_COMMIT_NUMBER,String.valueOf(commitNumber++));
        expectedMap.put(KEY_OPERATION,Operation.CREATE_OBJECT.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.put(KEY_OBJECT_ID,String.valueOf(objectId));
        expectedMap.put(KEY_JSON_DATA,object);
        String commit1 = gson.toJson(expectedMap);
        expectedMap.put(KEY_DATE,date);
        expectedMap.put(KEY_COMMIT_NUMBER,String.valueOf(commitNumber++));
        expectedMap.put(KEY_OPERATION,Operation.CREATE_COLLECTION.toString());
        expectedMap.put(KEY_COLLECTION,collectionName);
        expectedMap.remove(KEY_OBJECT_ID);
        expectedMap.put(KEY_JSON_DATA,collectionSchema);
        String commit2= gson.toJson(expectedMap);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(commit0);
            writer.newLine();
            writer.write(commit1);
            writer.newLine();
            writer.write(commit2);
            writer.newLine();
        }
        ArrayList<String> expected = new ArrayList<>();
        expected.add(commit1);
        expected.add(commit2);
        Assert.assertEquals(expected, log.getCommitsAfter(0));
        file.delete();
    }
    @Test(expected = CommitLogException.class)
    public void testGetCommitsAfterNegativeInputParameter() throws CommitLogException {
        log.getCommitsAfter(-1);
    }


    @BeforeClass
    public static void createFile() throws IOException {
        file.createNewFile();
        log=new FileCommitLog(fileName, "yyyy.MM.dd 'at' HH:mm:ss z");
    }

    @AfterClass
    public static void deleteFile() {
        file.delete();

    }

    private Map<String, String> getLastCommitFieldsWithEmptyDateAndCommitNumber() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String lastLine = null;
            String line = null;
            while ((line = reader.readLine()) != null) {
                lastLine = line;
            }
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            Map<String,String> commitMap = gson.fromJson(lastLine,type);
            commitMap.put(KEY_DATE,EMPTY_FIELD);
            commitMap.put(KEY_COMMIT_NUMBER,EMPTY_FIELD);
            return commitMap;
        }


    }


}
