package by.training.node.businesslogic;

import by.training.caches.Cache;
import by.training.caches.LRUCache;
import by.training.node.commitlog.CommitLog;
import by.training.node.commitlog.FileCommitLog;
import by.training.node.dao.DAO;
import by.training.node.dao.DatabaseDAO;
import by.training.node.exсeptions.CommitLogException;
import by.training.node.exсeptions.DAOException;
import by.training.node.exсeptions.WrongInputDataException;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NodeBusinessLogicTest {
    private Cache<Integer,String> mockCache = Mockito.mock(LRUCache.class);
    private DAO mockDAO = Mockito.mock(DatabaseDAO.class);
    private CommitLog mockCommitLog = Mockito.mock(FileCommitLog.class);
    private NodeBusinessLogic nodeBusinessLogic = new NodeBusinessLogic(mockCache,mockDAO,mockCommitLog);

    private String testCollectionName= "testCollectionName";
    private String jsonSchema ="{\n" +
            "    \"definitions\": {},\n" +
            "    \"$schema\": \"http://json-schema.org/draft-07/schema#\",\n" +
            "    \"$id\": \"http://example.com/root.json\",\n" +
            "    \"type\": \"object\",\n" +
            "    \"title\": \"The Root Schema\",\n" +
            "    \"required\": [\n" +
            "      \"name\"\n" +
            "    ],\n" +
            "   \"properties\": {\n" +
            "     \"name\": {\n" +
            "       \"$id\": \"#/properties/name\",\n" +
            "       \"type\": \"string\",\n" +
            "       \"title\": \"The Name Schema\",\n" +
            "       \"default\": \"\",\n" +
            "       \"examples\": [\n" +
            "         \"name\"\n" +
            "       ],\n" +
            "       \"pattern\": \"^(.*)$\"\n" +
            "     }\n" +
            "   }\n" +
            " }\n";
    private String jsonObject="{\"name\":\"name\"}";
    @Test
    public void testCreateCollection() throws WrongInputDataException, DAOException, CommitLogException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(false);
        nodeBusinessLogic.createCollection(testCollectionName,jsonSchema);
        Mockito.verify(mockCommitLog).logCreateCollection(testCollectionName,jsonSchema);
     Mockito.verify(mockDAO).putCollection(testCollectionName,jsonSchema);
    }

    @Test(expected = WrongInputDataException.class)
    public void testCreateCollectionThrowsExceptionWhenWrongCollectionName() throws DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
     nodeBusinessLogic.createCollection(testCollectionName,"SomeSchema");
    }

    @Test
    public void testGetCollection() throws WrongInputDataException, DAOException, CommitLogException {
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("name","name");
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        when(mockDAO.getCollection(testCollectionName)).thenReturn(resultMap);

        Assert.assertEquals(jsonObject,nodeBusinessLogic.
                readCollection(testCollectionName));
    }

    @Test(expected = WrongInputDataException.class)
    public void testGetCollectionThrowsExceptionWhenWrongCollectionName() throws
            DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(false);
        nodeBusinessLogic.readCollection(testCollectionName);
    }
    @Test
    public void testCreateObject() throws WrongInputDataException, DAOException, CommitLogException {

        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        when(mockDAO.getCollectionSchema(testCollectionName)).thenReturn(jsonSchema);
        when(mockDAO.getId()).thenReturn(1);
        int id=nodeBusinessLogic.createObject(testCollectionName,jsonObject);
        Mockito.verify(mockCommitLog).logCreateObject(testCollectionName,id,jsonObject);
        Mockito.verify(mockCache).putInCache(new Integer(1),jsonObject);
        Mockito.verify(mockDAO).putObject(testCollectionName,1,jsonObject);
        Assert.assertEquals(1,id);
    }

    @Test(expected = WrongInputDataException.class)
    public void testCreateObjectThrowsExceptionWhenWrongJSONFile() throws
            DAOException, WrongInputDataException {
        when(mockDAO.getCollectionSchema(testCollectionName)).thenReturn(jsonSchema);
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        nodeBusinessLogic.createObject(testCollectionName,"{\"nam\":\"nam\" }");
    }

    @Test(expected = WrongInputDataException.class)
    public void testCreateObjectThrowsExceptionWhenWrongCollectionName() throws
            DAOException, WrongInputDataException {
        when(mockDAO.getCollectionSchema(testCollectionName)).thenReturn(jsonSchema);
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(false);
        nodeBusinessLogic.createObject(testCollectionName,jsonObject);
    }

    @Test
    public void testGetObjectFromCache() throws WrongInputDataException, DAOException, CommitLogException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        when(mockCache.getFromCache(1)).thenReturn(jsonObject);
        when(mockDAO.getObject(testCollectionName,1)).thenReturn(null);
        Assert.assertEquals(jsonObject,nodeBusinessLogic.readObjectFromCache(testCollectionName,"1"));


    }
    @Test
    public void testGetObjectFromDAO() throws WrongInputDataException, DAOException, CommitLogException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        when(mockCache.getFromCache(1)).thenReturn(null);
        when(mockDAO.getObject(testCollectionName,1)).thenReturn(jsonObject);
        Assert.assertEquals(jsonObject,nodeBusinessLogic.readObjectFromDAO(testCollectionName,"1"));

    }


    @Test(expected = WrongInputDataException.class)
    public void testGetObjectThrowsExceptionWhenWrongId() throws DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        nodeBusinessLogic.readObjectFromCache(testCollectionName,"a");
    }
    @Test(expected = WrongInputDataException.class)
    public void testGetObjectThrowsExceptionWhenWrongCollectionName() throws DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(false);
        nodeBusinessLogic.readObjectFromCache(testCollectionName,"1");
    }

    @Test
    public void testUpdateObject() throws WrongInputDataException, DAOException, CommitLogException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        when(mockCache.getFromCache(1)).thenReturn(null);
        when(mockDAO.getObject(testCollectionName,1)).thenReturn("{\"name\":\"object\",\"number\":\"1\"}");
        nodeBusinessLogic.updateObject(testCollectionName,"1","{\"name\":\"name\"}");
        Mockito.verify(mockCommitLog).logUpdateObject(testCollectionName,1,"{\"name\":\"name\",\"number\":\"1\"}");
        Mockito.verify(mockCache).updateValue(1,"{\"name\":\"name\",\"number\":\"1\"}");
        Mockito.verify(mockDAO).updateObject(testCollectionName,1,"{\"name\":\"name\",\"number\":\"1\"}");
    }


    @Test(expected = WrongInputDataException.class)
    public void testUpdateObjectThrowsExceptionWhenWrongId() throws DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        nodeBusinessLogic.updateObject(testCollectionName,"a",jsonObject);
    }
    @Test(expected = WrongInputDataException.class)
    public void testUpdateObjectThrowsExceptionWhenWrongCollectionName() throws DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(false);
        nodeBusinessLogic.updateObject(testCollectionName,"1",jsonObject);
    }


    @Test
    public void testUpdateCollection() throws DAOException, WrongInputDataException, CommitLogException {
        String newSchema="newSchema";
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        nodeBusinessLogic.updateCollection(testCollectionName,newSchema);
        Mockito.verify(mockCommitLog).logUpdateCollection(testCollectionName,newSchema);
        verify(mockDAO).deleteAllObjectsExceptSchema(testCollectionName);
        verify(mockDAO).updateObject(testCollectionName,0,newSchema);
    }

    @Test(expected = WrongInputDataException.class)
    public void testUpdateCollectionThrowsExceptionWhenWrongCollectionName() throws
            DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(false);
        nodeBusinessLogic.updateCollection(testCollectionName,"newSchema");
    }



    @Test
    public void testDeleteObject() throws WrongInputDataException, DAOException, CommitLogException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        nodeBusinessLogic.deleteObject(testCollectionName,"1");
        Mockito.verify(mockCommitLog).logDeleteObject(testCollectionName,1);
        Mockito.verify(mockCache).deleteFromCache(new Integer(1));
        Mockito.verify(mockDAO).deleteObject(testCollectionName,1);
    }

    @Test(expected = WrongInputDataException.class)
    public void testDeleteObjectThrowsExceptionWhenWrongId() throws DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        nodeBusinessLogic.deleteObject(testCollectionName,"a");
    }
    @Test(expected = WrongInputDataException.class)
    public void testDeleteObjectThrowsExceptionWhenWrongCollectionName() throws DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(false);
        nodeBusinessLogic.deleteObject(testCollectionName,"1");
    }

    @Test
    public void testDeleteCollection() throws WrongInputDataException, DAOException, CommitLogException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(true);
        nodeBusinessLogic.deleteCollection(testCollectionName);
        Mockito.verify(mockCommitLog).logDeleteCollection(testCollectionName);
        Mockito.verify(mockDAO).deleteCollection(testCollectionName);
    }

    @Test(expected = WrongInputDataException.class)
    public void testDeleteCollectionThrowsExceptionWhenWrongCollectionName() throws
            DAOException, WrongInputDataException {
        when(mockDAO.containsCollection(testCollectionName)).thenReturn(false);
        nodeBusinessLogic.deleteCollection(testCollectionName);
    }
}
