package by.training.node.servlets;

import by.training.node.businesslogic.NodeBusinessLogic;
import by.training.node.exсeptions.DAOException;
import by.training.node.exсeptions.WrongInputDataException;
import by.training.node.servlets.logic.JSONLogic;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;

import static org.mockito.Mockito.*;

public class CollectionServletTest {

        private static String testCollectionName= "testCollectionName";
        private static String jsonObject="{\"name\":\"name\"}";
        private static String jsonSchema="someSchema";
        private static String id ="1";
        private static HttpServletRequest req = mock(HttpServletRequest.class);
        private static HttpServletResponse resp = mock(HttpServletResponse.class);
        private static NodeBusinessLogic mockBusinessLogic = mock(NodeBusinessLogic.class);
        private static JSONLogic mockJSONLogic = mock(JSONLogic.class);
        private static CollectionsServlet collectionsServlet = new CollectionsServlet();


        @BeforeClass
        public static void initTest() throws NoSuchFieldException, IllegalAccessException {
            Field businessLogicField =collectionsServlet.getClass().getDeclaredField("nodeBusinessLogic");
            businessLogicField.setAccessible(true);
            businessLogicField.set(collectionsServlet,mockBusinessLogic);
            Field jsonLogicField = collectionsServlet.getClass().getDeclaredField("jsonLogic");
            jsonLogicField.setAccessible(true);
            jsonLogicField.set(collectionsServlet,mockJSONLogic);
            when(req.getPathInfo()).thenReturn("/"+testCollectionName);
        }
        @Test
        public void testDoGet() throws ServletException, IOException, WrongInputDataException, DAOException {
            String jsonCollection="jsonCollection";
            when(mockBusinessLogic.readCollection(testCollectionName)).thenReturn(jsonCollection);
            when(resp.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
            collectionsServlet.doGet(req,resp);
            verify(mockBusinessLogic).readCollection(testCollectionName);
            verify(mockJSONLogic).sendJsonInResponse(resp,jsonCollection);
        }
        @Test
        public void testDoPost() throws IOException, WrongInputDataException, DAOException, ServletException {
            when(mockJSONLogic.getJsonFromRequest(req)).thenReturn(jsonSchema);
            collectionsServlet.doPost(req,resp);
            verify(mockBusinessLogic).createCollection(testCollectionName,jsonSchema);

        }
        @Test
        public void testDoPostWhenNullJSON() throws IOException, WrongInputDataException, DAOException, ServletException {
            HttpServletResponse resp = mock(HttpServletResponse.class);
            when(mockJSONLogic.getJsonFromRequest(req)).thenReturn(null);
            collectionsServlet.doPost(req,resp);
            verify(resp).sendError(HttpServletResponse.SC_BAD_REQUEST,"JSON file not found");

        }
        @Test
        public void testDoDelete() throws WrongInputDataException, DAOException, ServletException, IOException {
            collectionsServlet.doDelete(req,resp);
            verify(mockBusinessLogic).deleteCollection(testCollectionName);
        }
        @Test
        public void testDoPut() throws IOException, WrongInputDataException, DAOException, ServletException {
            BufferedReader br =new BufferedReader(new StringReader(jsonSchema));
            when(req.getReader()).thenReturn(br);
            when(mockJSONLogic.getJsonFromRequest(req)).thenReturn(jsonSchema);
            collectionsServlet.doPut(req,resp);
            verify(mockJSONLogic).getJsonFromRequest(req);
            verify(mockBusinessLogic).updateCollection(testCollectionName,jsonSchema);

        }
        @Test
        public void testDoPutWhenNullJSON() throws IOException, ServletException {
            HttpServletResponse resp = mock(HttpServletResponse.class);
            when(mockJSONLogic.getJsonFromRequest(req)).thenReturn(null);
            collectionsServlet.doPut(req,resp);
            verify(resp).sendError(HttpServletResponse.SC_BAD_REQUEST,"JSON file not found");

        }

    }


