package by.training.node.servlets;

import by.training.node.businesslogic.NodeBusinessLogic;
import by.training.node.exсeptions.DAOException;
import by.training.node.exсeptions.WrongInputDataException;
import by.training.node.servlets.logic.JSONLogic;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;

import static org.mockito.Mockito.*;

public class ObjectServletTest {
    private static String testCollectionName= "testCollectionName";
    private static String jsonObject="{\"name\":\"name\"}";
    private static String id ="1";
    private static HttpServletRequest req = mock(HttpServletRequest.class);
    private static HttpServletResponse resp = mock(HttpServletResponse.class);
    private static NodeBusinessLogic mockBusinessLogic = mock(NodeBusinessLogic.class);
    private static JSONLogic mockJSONLogic = mock(JSONLogic.class);
    private static ObjectsServlet objectsServlet = new ObjectsServlet();


    @BeforeClass
    public static void initTest() throws NoSuchFieldException, IllegalAccessException {
        Field businessLogicField =objectsServlet.getClass().getDeclaredField("nodeBusinessLogic");
        businessLogicField.setAccessible(true);
        businessLogicField.set(objectsServlet,mockBusinessLogic);
        Field jsonLogicField = objectsServlet.getClass().getDeclaredField("jsonLogic");
        jsonLogicField.setAccessible(true);
        jsonLogicField.set(objectsServlet,mockJSONLogic);
        when(req.getPathInfo()).thenReturn("/"+testCollectionName+"/"+id);
    }
    @Test
    public void testDoGet() throws ServletException, IOException, WrongInputDataException, DAOException {
        String jsonCollection="jsonCollection";
      //  when(mockBusinessLogic.readObject(testCollectionName,id)).thenReturn(jsonCollection);
        when(resp.getWriter()).thenReturn(new PrintWriter(new StringWriter()));
        objectsServlet.doGet(req,resp);
        //verify(mockBusinessLogic).readObject(testCollectionName,id);
        verify(mockJSONLogic).sendJsonInResponse(resp,jsonCollection);
    }
    @Test
    public void testDoPost() throws IOException, WrongInputDataException, DAOException, ServletException {
        when(req.getRequestURL()).thenReturn(new StringBuffer("url"));
    when(mockJSONLogic.getJsonFromRequest(req)).thenReturn(jsonObject);
    objectsServlet.doPost(req,resp);
    verify(mockBusinessLogic).createObject(testCollectionName,jsonObject);

    }
    @Test
    public void testDoPostWhenNullJSON() throws IOException, WrongInputDataException, DAOException, ServletException {
        HttpServletResponse resp = mock(HttpServletResponse.class);
        when(mockJSONLogic.getJsonFromRequest(req)).thenReturn(null);
        objectsServlet.doPost(req,resp);
        verify(resp).sendError(HttpServletResponse.SC_BAD_REQUEST,"JSON file not found");

    }
    @Test
    public void testDoDelete() throws WrongInputDataException, DAOException, ServletException, IOException {
        objectsServlet.doDelete(req,resp);

        verify(mockBusinessLogic).deleteObject(testCollectionName,id);
    }
    @Test
    public void testDoPut() throws IOException, WrongInputDataException, DAOException, ServletException {
        BufferedReader br =new BufferedReader(new StringReader(jsonObject));
        when(req.getReader()).thenReturn(br);
        when(mockJSONLogic.getJsonFromRequest(req)).thenReturn(jsonObject);
        objectsServlet.doPut(req,resp);
        verify(mockJSONLogic).getJsonFromRequest(req);
        verify(mockBusinessLogic).updateObject(testCollectionName,id,jsonObject);

    }
    @Test
    public void testDoPutWhenNullJSON() throws IOException, WrongInputDataException, DAOException, ServletException {
        HttpServletResponse resp = mock(HttpServletResponse.class);
        when(mockJSONLogic.getJsonFromRequest(req)).thenReturn(null);
        objectsServlet.doPut(req,resp);
        verify(resp).sendError(HttpServletResponse.SC_BAD_REQUEST,"JSON file not found");

    }

}
