package by.training.node.servlets.logic;

import by.training.node.servlets.logic.JSONLogic;
import org.junit.Assert;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JSONLogicTest {
    private static HttpServletRequest req = mock(HttpServletRequest.class);
    private static HttpServletResponse resp = mock(HttpServletResponse.class);
    private String jsonObject="{\"name\":\"name\"}";
    private JSONLogic jsonLogic = new JSONLogic();
    @Test
    public void testGetJsonFromRequest() throws IOException {
        when(req.getReader()).thenReturn(new BufferedReader(new StringReader(jsonObject)));
        String resultObject =jsonLogic.getJsonFromRequest(req);
        Assert.assertEquals(jsonObject,resultObject);
    }
    @Test
    public void testSendJsonInResponse() throws IOException {
        StringWriter stringWriter =new StringWriter();
        PrintWriter printWriter =new PrintWriter(stringWriter);
        when(resp.getWriter()).thenReturn(printWriter);
        jsonLogic.sendJsonInResponse(resp,jsonObject);
        Assert.assertEquals(jsonObject,stringWriter.toString());
    }
}
