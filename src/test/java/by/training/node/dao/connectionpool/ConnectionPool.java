package by.training.node.dao.connectionpool;

import by.training.node.exсeptions.DAOException;

import java.sql.Connection;

/**
 * Interface that represents pool of connections to database.
 */
public interface ConnectionPool {
    /**
     * Method for get connection from pool.
     * @return - connection.
     * @throws DAOException - if an error occurred while getting the connection from pool.
     */
    public  Connection getConnection() throws DAOException;
}
