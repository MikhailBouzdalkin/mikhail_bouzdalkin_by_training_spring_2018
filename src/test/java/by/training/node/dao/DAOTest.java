package by.training.node.dao;

import by.training.node.dao.connectionpool.ConnectionPool;
import by.training.node.dao.connectionpool.TomcatConnectionPool;
import by.training.node.exсeptions.DAOException;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.*;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.when;

public class DAOTest {
    private static final String URL="jdbc:postgresql://localhost:5433/node1";
    private static final String USER="postgres";
    private static final String PASSWORD="root";


    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(DAOTest.class);
    private static ConnectionPool mockPool = Mockito.mock(TomcatConnectionPool.class);
    private static String testCollectionName="testexamplecollection";
    private static  String jsonCollectionSchema= "schema";

    private static DAO dao = new DatabaseDAO(mockPool);

    @Test
    public void testPutAndGetCollection()throws SQLException {
        try(
            Connection connectionGetTable =DriverManager.
                    getConnection(URL,USER,PASSWORD);
            Connection connectionGetId =DriverManager.
                getConnection(URL,USER,PASSWORD);
            Connection connection =DriverManager.
                    getConnection(URL,USER,PASSWORD);
            Connection connectionPutTable =DriverManager.
                    getConnection(URL,USER,PASSWORD)) {
            when(mockPool.getConnection()).thenReturn(connectionGetId);
            int id = dao.getId();
            when(mockPool.getConnection()).thenReturn(connectionPutTable);
            dao.putCollection("testputandgetcollection", jsonCollectionSchema);
            connection.createStatement().executeUpdate
                    ("INSERT INTO testputandgetcollection(id,object) VALUES("+id+",'string')");
            when(mockPool.getConnection()).thenReturn(connectionGetTable);
                Map<String,String> resultMap=dao.getCollection("testputandgetcollection");
            connection.createStatement().executeUpdate
                    ("DROP TABLE testputandgetcollection;");
            Assert.assertEquals("string",resultMap.get(String.valueOf(id)));
            Assert.assertEquals(1,resultMap.size());

            } catch (DAOException e) {
                log.error(e.getMessage(), e);
            }
    }

         @Test
         public void testPutAndGetObject() throws SQLException {
         String object= "objectInJSONString";
        try(Connection connectionGetId =DriverManager.
                     getConnection(URL,USER,PASSWORD);
                 Connection connectionGetObject =DriverManager.
                         getConnection(URL,USER,PASSWORD);
                 Connection connectionPutObject =DriverManager.
                getConnection(URL,USER,PASSWORD);) {
                 when(mockPool.getConnection()).thenReturn(connectionGetId);
                 int key = dao.getId();
                 when(mockPool.getConnection()).thenReturn(connectionPutObject);
                 dao.putObject(testCollectionName,key,object);
                when(mockPool.getConnection()).thenReturn(connectionGetObject);
                String resultObject=dao.getObject(testCollectionName,key);
                Assert.assertEquals(resultObject,object);
            } catch (DAOException e) {
                log.error(e.getMessage(),e);
            }


         }

         @Test
         public void testGetCollectionSchema() throws SQLException {
             try(Connection connectionGetSchema =DriverManager.
                     getConnection(URL,USER,PASSWORD)){

                 when(mockPool.getConnection()).thenReturn(connectionGetSchema);
                 Assert.assertEquals(dao.getCollectionSchema(testCollectionName),jsonCollectionSchema);

             } catch (DAOException e) {
                 log.error(e.getMessage(),e);
             }

         }
         @Test
         public void testExistsCollection() throws SQLException {
             try(Connection connectionExistsCollection =DriverManager.
                     getConnection(URL,USER,PASSWORD)){
                 when(mockPool.getConnection()).thenReturn(connectionExistsCollection);
                 Assert.assertTrue(dao.containsCollection(testCollectionName));
             } catch (DAOException e) {
                 log.error(e.getMessage(),e);
             }
         }
         @Test
         public void testNotExistsCollection() throws SQLException{
         String additionalName="1";
             try(Connection connectionNotExistsCollection =DriverManager.
                     getConnection(URL,USER,PASSWORD);
                 Connection connectionDropTable =DriverManager.
                         getConnection(URL,USER,PASSWORD)){
                 connectionDropTable.createStatement().executeUpdate("DROP TABLE IF EXISTS "
                         +testCollectionName+additionalName+";");
                 when(mockPool.getConnection()).thenReturn(connectionNotExistsCollection);
                 Assert.assertFalse(dao.containsCollection(testCollectionName+additionalName));
             } catch (DAOException e) {
                 log.error(e.getMessage(),e);
             }
         }
         @Test
         public void testDeleteCollection() throws SQLException {

             try(Connection connectionCreateTable =DriverManager.
                     getConnection(URL,USER,PASSWORD);
             Connection connectionDeleteTable = DriverManager.getConnection(URL,USER,PASSWORD);
             Connection connectionExistsCollection =DriverManager.
                     getConnection(URL,USER,PASSWORD)){
                 when(mockPool.getConnection()).thenReturn(connectionDeleteTable);
                 connectionCreateTable.createStatement().executeUpdate("CREATE TABLE testCreateTable(id int," +
                         " object text);");
                 dao.deleteCollection("testCreateTable");
                 boolean tableExists=true;
                 try (ResultSet rs=connectionExistsCollection.createStatement().executeQuery("SELECT to_regclass('testCreateTable');")){
                     if(rs.next()){
                         tableExists =rs.getBoolean(1);
                     }
                 }
                 Assert.assertFalse(tableExists);

             } catch (DAOException e) {
                 log.error(e.getMessage(),e);
             }
         }
         @Test
         public void testDeleteObject() throws SQLException {
             try(Connection connection =DriverManager.
                     getConnection(URL,USER,PASSWORD);
             Connection connectionGetId=DriverManager.
                     getConnection(URL,USER,PASSWORD);
             Connection connectionCcheckExistObject=DriverManager.
                     getConnection(URL,USER,PASSWORD)){
                 when(mockPool.getConnection()).thenReturn(connectionGetId);
                 int id = dao.getId();
                 connection.createStatement().executeUpdate
                         ("INSERT INTO "+testCollectionName+"(id,object) VALUES("+id+",'string')");
                 when(mockPool.getConnection()).thenReturn(connection);
                 dao.deleteObject(testCollectionName,id);
                 try(ResultSet rs=connectionCcheckExistObject.createStatement().
                         executeQuery("SELECT EXISTS(SELECT id from "
                         +testCollectionName+" WHERE id="+id+");")){
                     boolean objectExists=true;
                     if(rs.next()){
                         objectExists =rs.getBoolean(1);
                     }
                     Assert.assertFalse(objectExists);
                 }

                 } catch (DAOException e) {
                 log.error(e.getMessage(),e);
             }
         }
         @Test
         public void testUpdateObject() throws SQLException {
             try(Connection connection =DriverManager.
                     getConnection(URL,USER,PASSWORD);
                 Connection connectionGetId=DriverManager.
                     getConnection(URL,USER,PASSWORD);
                 Connection connectionGetUpdatedObject=DriverManager.
                         getConnection(URL,USER,PASSWORD)){
                 when(mockPool.getConnection()).thenReturn(connectionGetId);
                 int id = dao.getId();
                 connection.createStatement().executeUpdate("INSERT INTO "+testCollectionName+
                         "(id,object) VALUES("+id+",'Json')");
                 when(mockPool.getConnection()).thenReturn(connection);
                 dao.updateObject(testCollectionName,id,"newJson");
                 try(ResultSet rs=connectionGetUpdatedObject.createStatement().
                         executeQuery("SELECT object from "
                                 +testCollectionName+" WHERE id="+id+";")){
                     String newObject=null;
                     if(rs.next()){
                         newObject = rs.getString(1);
                     }
                     Assert.assertEquals("newJson",newObject);
                 }
             } catch (DAOException e) {
                 log.error(e.getMessage(),e);
             }
         }

         @Test
         public void deleteAllObjectsExceptSchema() throws SQLException {
             try (Connection connection = DriverManager.
                     getConnection(URL, USER, PASSWORD);
                  Connection connectionGetRowsCount = DriverManager.
                          getConnection(URL, USER, PASSWORD);
                  Connection connectionGetId = DriverManager.
                          getConnection(URL, USER, PASSWORD);
                  Statement st = connectionGetRowsCount.createStatement()){
                 when(mockPool.getConnection()).thenReturn(connectionGetId);
                 int id = dao.getId();
                 connection.createStatement().executeUpdate("INSERT INTO " + testCollectionName
                         + "(id,object) VALUES (" + id + ",'jsonObject');");
                 when(mockPool.getConnection()).thenReturn(connection);
                 dao.deleteAllObjectsExceptSchema(testCollectionName);
                 try (ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM "+testCollectionName)) {
                     long rowCount = 0;
                     if (rs.next()) {
                         rowCount = rs.getLong(1);
                     }
                     Assert.assertEquals(1, rowCount);
                 }
             } catch (DAOException e) {
                 log.error(e.getMessage(),e);
             }
         }
         @Test
         public void testContainsObjectNotExists() throws SQLException {
             try (Connection connection = DriverManager.
                     getConnection(URL, USER, PASSWORD);
                  Connection connectionGetId = DriverManager.
                          getConnection(URL, USER, PASSWORD);
                  Statement st = connection.createStatement()){
                 when(mockPool.getConnection()).thenReturn(connectionGetId);
                 int id = dao.getId();
                 when(mockPool.getConnection()).thenReturn(connection);
                 Assert.assertFalse(dao.containsObject(testCollectionName,id));
             } catch (DAOException e) {
                 log.error(e.getMessage(),e);
             }
         }
         @Test
         public void testContainsObjectExists() throws SQLException {
             try (Connection connection = DriverManager.
                     getConnection(URL, USER, PASSWORD);
                  Connection connectionGetId = DriverManager.
                          getConnection(URL, USER, PASSWORD)) {
                 when(mockPool.getConnection()).thenReturn(connectionGetId);
                 int id = dao.getId();
                 connection.createStatement().executeUpdate("INSERT INTO " + testCollectionName
                         + "(id,object) VALUES (" + id + ",'jsonObject');");
                 when(mockPool.getConnection()).thenReturn(connection);
                 Assert.assertTrue(dao.containsObject(testCollectionName, id));
             } catch (DAOException e) {
                 log.error(e.getMessage(), e);
             }
         }


         @Test
         public void testGetCollectionNames() throws SQLException {
             try (Connection connection = DriverManager.
                     getConnection(URL, USER, PASSWORD)){
                 when(mockPool.getConnection()).thenReturn(connection);
                     Set<String> collectionNames = dao.getCollectionNames();
                     Assert.assertTrue(collectionNames.contains(testCollectionName));
                 } catch (DAOException e) {
                     log.error(e.getMessage(), e);
                 }

         }

         @BeforeClass
    public static void createTestCollection() throws SQLException {
             try(Connection connectionPutTable =DriverManager.
                     getConnection(URL,USER,PASSWORD);
                 Connection connectionInsertSchema =DriverManager.
                         getConnection(URL,USER,PASSWORD)) {
                 when(mockPool.getConnection()).thenReturn(connectionPutTable);
                 dao.putCollection(testCollectionName, jsonCollectionSchema);
             } catch (DAOException e) {
                 log.error(e.getMessage(), e);
             }
         }

         @AfterClass
      public static void deleteTestCollection() throws SQLException {
        try(Connection connectionDropTable =DriverManager.
                getConnection(URL,USER,PASSWORD)){
            connectionDropTable.createStatement().executeUpdate("DROP TABLE " +testCollectionName+";");
        }
         }

}
