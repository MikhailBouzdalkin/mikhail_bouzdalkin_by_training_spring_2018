package by.training.servlet;

import by.training.servlet.businesslogic.BusinessLogic;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mockito.internal.matchers.Null;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.Map;

public class BusinessLogicTest {

    private ApplicationContext springAppLicationContext = new ClassPathXmlApplicationContext("spring.xml");
    private BusinessLogic businessLogic = (BusinessLogic)springAppLicationContext.getBean("businessLogic");


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testDeserializeAndPutInCacheThrownExceptionIfMapIsEmpty() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        thrown.expect(NullPointerException.class);
        businessLogic.deserializeAndPutInCache(new HashMap<String, String>());
    }
    @Test
    public void testDeserializeAndPutInCacheThrownExceptionIfClassInMapNotExists() throws IllegalAccessException, ClassNotFoundException, InstantiationException {

        Map<String,String> map =new HashMap<>();
        map.put("Class","NonExist");
        thrown.expect(ClassNotFoundException.class);
        businessLogic.deserializeAndPutInCache(map);
    }
    @Test
    public void testGetFromCacheAndSerializeThrownExceptionWhenGetElementWithNonExistKey() throws IllegalAccessException {
        Map<String,String> map =new HashMap<>();
        thrown.expect(NullPointerException.class);
        businessLogic.getFromCacheAndSerialize("nonExistKey");
    }
    @Test
    public void testGetFromCacheAndSerialize() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        Map<String,String> map =new HashMap<>();
        map.put("Class","by.training.servlet.beans.UserData");
        map.put("name","vasya");
        map.put("surname","vasyliev");
        map.put("id","1");
        businessLogic.deserializeAndPutInCache(map);
        Map<String,String> resultMap=businessLogic.getFromCacheAndSerialize("1");
        Assert.assertArrayEquals(map.keySet().toArray(),resultMap.keySet().toArray());
        Assert.assertArrayEquals(map.values().toArray(),resultMap.values().toArray());
    }


}
