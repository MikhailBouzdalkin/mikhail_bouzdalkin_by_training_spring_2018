package by.training.junit;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverterTest {
    private static final String DATE_PATTERN = "dd.mm.yyyy";
    DateConverter converter = new DateConverter(DATE_PATTERN);

    @Test
    public void testGetDate() throws ParseException {
        String dateString = "24.06.2018";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        Date expectedDate = dateFormat.parse(dateString);
        Date actualDate = converter.getDate(dateString);
        Assert.assertEquals(expectedDate, actualDate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDateNullParameter() {
        converter.getDate(null);
    }

    @Test
    public void testGetDateString() throws ParseException {
        String expectedDateString = "24.06.2018";
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        Date date = dateFormat.parse(expectedDateString);
        String actualDateString = converter.getDateString(date);
        Assert.assertEquals(expectedDateString, actualDateString);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDateStringNullParameter() {
        converter.getDateString(null);
    }


}
