package by.training.junit;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class StringUtilTest {
    private static final String KEY_NAME="name";
    private static final String KEY_SURNAME="surname";
    private static IDAO dao = mock(DAOImplementation.class);

    @Before
    public void initBeforeTest() {
        dao = mock(DAOImplementation.class);
        when(dao.get(KEY_NAME)).thenReturn("Vasya");
        when(dao.get(KEY_SURNAME)).thenReturn("Pupkin");
    }


    @Test
    public void testModify() {
        StringUtil stringUtil = new StringUtil(dao);
        stringUtil.modify(KEY_NAME, "1");
        verify(dao).get(KEY_NAME);
        verify(dao).remove(KEY_NAME);
        verify(dao).put(KEY_NAME, "Vasya1");


    }


}
