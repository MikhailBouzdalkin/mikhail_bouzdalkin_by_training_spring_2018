package by.training.elevator;


import by.training.elevator.elevator.Elevator;
import by.training.elevator.exceptions.IncorrectConfigurationValueException;
import by.training.elevator.passengers.Passenger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;


public class ElevatorTest {
    Elevator.Controller controller;
    Elevator elevator;
    List<Passenger> elevatorContainer;
    @Before
    public  void initBeforeTest() throws NoSuchFieldException, IllegalAccessException,
            IOException, IncorrectConfigurationValueException {
        controller = new Elevator.Controller();
        controller.run();
        elevator= controller.getElevator();
        Field fieldElevatorContainer = elevator.getClass().getDeclaredField("evelvatorContainer");
        fieldElevatorContainer.setAccessible(true);
        elevatorContainer= (List<Passenger>)fieldElevatorContainer.get(elevator);
    }

    @Test
    public void testGetIntoElevator() throws NoSuchFieldException, IllegalAccessException {
        controller.getIntoElevator(new Passenger(1,1,2));
        Assert.assertEquals(elevatorContainer.size(),1);
    }
    @Test
    public void testGetOutOfElevator(){
        Passenger passenger =new Passenger(1,1,2);
        controller.getIntoElevator(passenger);
        controller.getOutOfElevator(passenger);
        Assert.assertEquals(elevatorContainer.size(),0);
    }



}
