package by.training.elevator;


import by.training.elevator.passengers.TransportationTask;
import org.junit.Assert;
import org.junit.Test;


public class TransportationTaskTest {
    @Test
    public void testNotifyTask(){
        TransportationTask transportationTask =new TransportationTask();
        transportationTask.start();
        transportationTask.notifyTask();
        Assert.assertEquals(Thread.State.RUNNABLE,transportationTask.getState());
    }


}
