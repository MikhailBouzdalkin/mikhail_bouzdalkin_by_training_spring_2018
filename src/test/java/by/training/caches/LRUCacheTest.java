package by.training.caches;

import org.junit.Assert;
import org.junit.Test;

public class LRUCacheTest{
         LRUCache<Integer, String> cache =new LRUCache(2);
@Test
public void testMaxSize(){
    cache=new LRUCache(2);
    cache.putInCache(1,"A");
    cache.putInCache(2,"B");
    cache.putInCache(3,"C");
    cache.putInCache(4,"D");
    cache.getFromCache(1);
    Assert.assertNull(cache.getFromCache(2));
}
    @Test
    public void testLeast(){
    cache=new LRUCache(2);
        cache.putInCache(1,"A");
        cache.putInCache(2,"B");
        cache.putInCache(3,"C");
        cache.getFromCache(2);
        cache.putInCache(4,"D");
        Assert.assertNull(cache.getFromCache(2));
    }

}
