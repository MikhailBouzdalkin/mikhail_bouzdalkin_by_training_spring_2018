package by.training.caches;

import org.junit.Assert;
import org.junit.Test;

public class LFUCacheTest {
    LFUCache<Integer, String> cache;

    @Test
    public void testMaxSize(){
        cache =new LFUCache(2);
        cache.putInCache(1,"A");
        cache.putInCache(2,"B");
        cache.putInCache(3,"C");
        cache.putInCache(4,"D");
        Assert.assertNull(cache.getFromCache(2));
   }

    @Test
    public void testSize(){
        cache =new LFUCache(2);
        cache.putInCache(1,"A");
        cache.putInCache(2,"B");
        cache.getFromCache(1);
        cache.getFromCache(1);
        cache.getFromCache(2);
        cache.putInCache(3,"C");
        Assert.assertNotNull(cache.getFromCache(1));
    }

}
